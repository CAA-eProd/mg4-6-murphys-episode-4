/********************************************************
Author(s):eChannel (rich - rrey@caasco.ca )
Project:Murphys 4 Mini Game
Location:
https://bitbucket.org/CAA-eProd/mg4-6-murphys-episode-4
********************************************************/


(function() {

window.matchGame = window.matchGame || {};
var matchGame = {
  init: function () {
    matchGame.createMatchObjects();
    matchGame.gameStartGame();
    matchGame.gameDisplay();
    matchGame.gameControls();
    matchGame.gameIE10Layout();
    console.log('Match game loaded');
  },

  playGame: false,
  gameOver: false,
  totalMatching: 0,
  startGameTime: 3,
  startGameTimeSec: 60,

  objDesc: {
  "ambulance": 'Ground ambulance' ,
  "baggage": 'Lost baggage' ,
  "dental": 'Medical dental insurance' ,
  "departure": 'Departure travel insurance' ,
  "documents": 'Lost documents' ,
  "medical": 'Emergency medical insurance' ,
  "vision": 'Vision eye care' ,
  "x-ray": 'Medical x-ray'
  },

  gameOverMessage: 'Uh oh! Time has run out.',
  gameWinMessage: 'Good job! You&rsquo;ve found all eight matches.',

  //objects to match
  objArray: [ "ambulance", "baggage", "dental", "departure", "documents", "medical", "vision", "x-ray" ],

  matchObjArray: [],

  shuffle: function(array) {
    //random shuffle array
    var random, newArray, i;
    for (i = array.length; i; i -= 1) {
        random = Math.floor(Math.random() * i);
        newArray = array[i - 1];
        array[i - 1] = array[random];
        array[random] = newArray;
    }
  },

  createMatchObjects: function() {
    // duplicate objects
    for (var i = 0; i < this.objArray.length; i++) {
      this.matchObjArray.push(this.objArray[i]+'-1', this.objArray[i]+'-2');
    }
    //shuffle them
     return matchGame.shuffle(this.matchObjArray);
  },

  gameMessage: function(theClass, textEffect, message, overlayEffect, add) {
    return '<div class="'+ theClass +'"><div class="col"><span class="matchName '+ textEffect +'">'+ message +'</span><div class="overlay '+ overlayEffect +'"></div>'+ add +'</div></div>'
  },

  gameStartGame: function () {
    var sTim = matchGame.startGameTime;
    var min = matchGame.startGameTime - 1;
    var sec = matchGame.startGameTimeSec;
    var stopTime = false;
    var time = min + ':' + sec;

    var gameTimeView = document.getElementById('gameTimer');
    gameTimeView.innerHTML = '<div id="gameTime"> '+ sTim +':00 </div>';

    countDown = function() {

      if(stopTime == false) {
        if (sec != 0) {
          sec--;
        } else {
          sec = 60;
        }
        if (min != 0) {
          if (sec == 0) {
            min--
          }
          if (min == 1 && sec == 1) {
            min = 0;
            sec = 60;
          }
        } else if (min == 0 && sec == 0) {
          //TIMED OUT
          //time = '0:00';
          min = 0;
          sec = 0;
          stopTime = true;
          $('#gameView').append(matchGame.gameMessage('gameLoseContainer','', matchGame.gameOverMessage ,'fadein', '<div id="gamePlayAgainBtn" class="cta"><ul><li>Play Again</li></ul></div>' ));
        }
      }

      if (sec < 10) {
        time = min + ':0' + sec;
      } else {
        time = min + ':' + sec;
      }

      return time;
    }

    //reset game to play again
    $(document).on('click','#gamePlayAgainBtn', function() {
      gameTimeView.innerHTML = '<div id="gameTime"> '+ matchGame.startGameTime +':00 </div>';
      min = matchGame.startGameTime - 1;
      sec = 60;
      stopTime = false;
      $('#gameView').empty();
      matchGame.gameDisplay();
      matchGame.totalMatching = 0;
      $('.gameLoseContainer, .gameMessageContainer').remove();
    });


    $('#gameStartBtn').click(function () {

        $('.gameMessageContainer, #gameSkipBtn, #gameStartScreen').remove();

        $('#gameContainer').css('margin-top', '50px');

        $('#gamePlayScreen').css('display', 'block');

        $('.flip-container').addClass('flip-container-hide');


        isMac = navigator.userAgent.indexOf('Mac') > 0;
        isFirefox = /Firefox/i.test(navigator.userAgent);

         if(isMac && isFirefox){
          $('gameCloseBtn').css('top', '570px');
         } else if (isMac) {
          // $('.gameCloseBtn').css('top','-620px');
         }

         if(isFirefox) {
            $('.flip-container').addClass('flip-container-hide-ff');
         }

    });


    $('.gameStartGameBtn').click(function () {

        $('.flip-container').removeClass('flip-container-hide');
        $('.flip-container').removeClass('flip-container-hide-ff');

        $('#played-game').attr('value', 'Yes');
        console.log("Played game set to true");


        var timeInterval = setInterval(function(){

          if (matchGame.gameOver == false) {

            gameTimeView.innerHTML = '<div id="gameTime">' + countDown() + '</div>';
            //console.log(matchGame.gameOver);

          } else {
            clearInterval(timeInterval);
            gameTimeView.innerHTML = '<div id="gameTime">' + countDown() + '</div>';
          }


        }, 1000);

        matchGame.playGame = true;

        $(this).remove();


    });

  },

  gameDisplay: function () {
    var gameView = document.getElementById('gameView');
    // display objects
    // console.log(matchGame.matchObjArray);
    // console.log(matchGame.matchObjArray.length);

    var a = 0;
    var rows = 4;
    // place objects as classes tagged on grid of cards
    for (var i = 0; i < matchGame.matchObjArray.length; i++) {

      gameView.innerHTML += '<div class="flip-container"><div class="card '+ matchGame.matchObjArray[i] +'"><span class="face front"></span><span class="face back"></span></div></div>';

      a++;

      if (a == rows) {
        gameView.innerHTML += '<br />';
        a = 0;
      }

    }

    //display matched item
    //$('#gameView').append('<div class="gameMessageContainer"><div class="overlay"></div></div>');

  },

  gameControls: function () {
    //var thisWasClicked;
    var clickNum = 0;
    var clicked1stObj, clicked2ndObj, match1stObj, match2ndObj;
    var sameCard = false;
    var disabledClick = false;

      $(document).on('click', '.card', function(e){ // if user clicks card then:
        //define clicked card
        var clickedObj = $(this).attr("class");
        clickedObj = clickedObj.slice(4).trim();
        //console.log("disabled click = " + disabledClick);

        resetStuff = function () {
          //reset click objects
          clicked1stObj, clicked2ndObj  = null;
          //reset click
          clickNum = 0;

          if (!matchGame.gameOver) {
            disabledClick = false;
          } else {
            disabledClick = true;
          }
        };


        if (disabledClick == false && matchGame.playGame == true && matchGame.gameOver == false) {

          if (clickNum === 0) { //if first click
            // & not the same card
            // console.log(sameCard);
            if (sameCard == false) {
              // show object
              var cardImage = clickedObj.slice(0, - 2);
              $(this).children('.back').append('<img src="/~/media/OM/murphys-game/ep4/images/minigame/'+ cardImage +'.png" width="100%"  />');
              $(this).parent().addClass('toBack');
              // get object name and store it for comparison
              clicked1stObj = clickedObj;
              //thisWasClicked = $(this).attr("class");
              clickNum++;
            }
          } else if (clickNum == 1) {
              //if second click

              // player must now click another

               if (clickedObj == clicked1stObj ) { //SAME CARD

                //console.log('same card'+ clickedObj);
                sameCard = true;

               } else { //if different card
                sameCard = false;
                disabledClick = true;
                //console.log('different card');
                  //strip numbers for matching purposes
                  match1stObj = clicked1stObj.slice(0, - 2);
                  match2ndObj = clickedObj.slice(0, - 2);

                  if (match1stObj == match2ndObj) { // MATCH
                  // if the next clicked object matches then:
                    disabledClick = true;
                    $("[class*='"+match1stObj+"']").removeClass('card');

                    var cardImage2 = clickedObj.slice(0, - 2);
                    $(this).children('.back').append('<img src="/~/media/OM/murphys-game/ep4/images/minigame/'+ cardImage2 +'.png" width="100%"  />');
                    $(this).parent().addClass('toBack');

                    matchGame.totalMatching++;

                    //display matched message

                    if (matchGame.objDesc[match1stObj]) {
                      iconDefinition = matchGame.objDesc[match1stObj];
                    }

                    $('#gameView').append(matchGame.gameMessage('gameMessageContainer','', iconDefinition ,'fadeinout', '' ));


                    setTimeout(function () {
                       $(".gameMessageContainer .matchName").fadeOut('slow');
                    }, 3000);

                    removeMessage = function () {
                      $('.gameMessageContainer').remove();
                    }

                    if (!matchGame.gameOver) {
                      setTimeout(removeMessage, 4900);
                    }

                    //console.log("total matching:" + matchGame.totalMatching + " total array:" + matchGame.objArray.length);

                    // if currentmatchGame.TotalMatching = to array.length game over and user wins!
                    // if timer exists (need to check if it does) and it runs out then game over
                    if (matchGame.totalMatching == matchGame.objArray.length) {
                    // add to currentmatchGame.TotalMatching
                      console.log("you win!");
                      matchGame.gameOver = true;

                      $('#game-won').attr('value', 'Yes');

                      $('#gameView').append(matchGame.gameMessage('gameWinContainer','fadein', matchGame.gameWinMessage ,'fadein', '<div id="gameContinueBtn" class="cta"><ul><li>Continue</li></ul></div>' ));
                      $('#mini-game .cta').removeClass('js-none');
                      $('#gameStartBtn').remove();


                      disabledClick = true;
                    }
                    // else {
                    setTimeout(resetStuff, 2500);
                    // }

                  } else { // NO MATCH

                    var cardImage3 = clickedObj.slice(0, - 2);
                    $(this).children('.back').append('<img src="/~/media/OM/murphys-game/ep4/images/minigame/'+ cardImage3 +'.png" width="100%"  />');
                    $(".card."+clickedObj).parent().addClass('toBack');

                    clicked2ndObj = clickedObj;
                    //console.log('2nd obj:'+clicked2ndObj);

                    // hide first clicked object
                    removeCardOne = function () {
                      $(".card."+clicked1stObj+" img").remove('');
                      $(".card."+clicked1stObj).parent().removeClass('toBack');
                      //console.log(".card."+clicked1stObj);
                    };
                    // hide second clicked object
                    removeCardTwo = function () {
                      $(".card."+clicked2ndObj+" img").remove('');
                      $(".card."+clicked2ndObj).parent().removeClass('toBack');
                      //console.log(".card."+clicked2ndObj);
                    };

                    setTimeout(removeCardOne, 1000);
                    setTimeout(removeCardTwo, 2000);
                    // reset
                    setTimeout(resetStuff, 2000);

                  }
                  //
                  // show object
               }

          }

        }

        // console.log("Times clicked:"+clickNum);
      }); //end card click


  }, //end game mechanics

  gameIE10Layout : function () {
    var isIE10 = false;
    /*@cc_on
        if (/^10/.test(@_jscript_version)) {
            isIE10 = true;
        }
    @*/
    console.log(isIE10);
    if (isIE10) {
      $("#gameContainer").addClass('ie10');
    }
  }


};



$(document).ready(function() { matchGame.init(); });

})()
