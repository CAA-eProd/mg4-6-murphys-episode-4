

// ..............................................................................

// Project: MG4 | The Murphys East Cost Caper - TI Game
// Date: 5/20/2016
// CAA SCO - E-Chan

// ..............................................................................


window.murphys = window.murphys || {};

murphys.shared = {

    init : function () {

        var imageSrcs   = [ "/~/media/OM/murphys-game/images/caa-logo.png",
            "/~/media/OM/murphys-game/ep4/images/text/murphys-title.png",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-intro.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/intro/grasslight.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/intro/beach-water.gif",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-1.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/city-scape-street.jpg",
            "/~/media/OM/murphys-game/ep4/images/bg/city-scape.jpg",
            "/~/media/OM/murphys-game/ep4/images/characters/bubble/bro.png",
            "/~/media/OM/murphys-game/ep4/images/characters/bubble/dad.png",
            "/~/media/OM/murphys-game/ep4/images/characters/bubble/dad-bro-sis.png",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-2.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/grass.jpg",
            "/~/media/OM/murphys-game/ep4/images/bg/house-brick.jpg",
            "/~/media/OM/murphys-game/ep4/images/characters/bubble/mom.png",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-3.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/island.png",
            "/~/media/OM/murphys-game/ep4/images/characters/bubble/ortegas.png",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-4.gif",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-5.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/resto.png",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-6.gif",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-7.gif",
            "/~/media/OM/murphys-game/ep4/images/bg/ships-beach.jpg",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-8.gif",
            "/~/media/OM/murphys-game/ep4/images/animations/sc-9.gif",
            "/~/media/OM/murphys-game/ep4/images/minigame/ambulance.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/baggage.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/bg-game.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/bg-start.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/card-cover.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/card-cover-disabled.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/close.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/correct.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/dental.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/departure.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/documents.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/incorrect.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/medical.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/time.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/vision.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/win.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/x-ray.png",
            "/~/media/OM/murphys-game/ep4/images/minigame/ambulance.png",
                          ];
        var images      = [ ];

       // murphys.shared.ga();
        murphys.shared.preloadImages( imageSrcs, images, murphys.shared.letsStart );
        console.log("Murphys game loaded")

    },

    letsStart : function () {

        $('#loading').hide();
        $('#fullpage').show();

        murphys.shared.fullPage();
        murphys.shared.isMobile();
        murphys.shared.questions();

        murphys.shared.formSettings();
        murphys.shared.formValidation();
        murphys.shared.formFieldAnimation();
        murphys.shared.buttons();

        murphys.shared.ga();

        $.fn.fullpage.reBuild();

    },

    isMobile : function () {

        (function(i){var e=/iPhone/i,n=/iPod/i,o=/iPad/i,t=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,r=/Android/i,d=/BlackBerry/i,s=/Opera Mini/i,a=/IEMobile/i,b=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,h=RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),c=function(i,e){return i.test(e)},l=function(i){var l=i||navigator.userAgent;this.apple={phone:c(e,l),ipod:c(n,l),tablet:c(o,l),device:c(e,l)||c(n,l)||c(o,l)},this.android={phone:c(t,l),tablet:!c(t,l)&&c(r,l),device:c(t,l)||c(r,l)},this.other={blackberry:c(d,l),opera:c(s,l),windows:c(a,l),firefox:c(b,l),device:c(d,l)||c(s,l)||c(a,l)||c(b,l)},this.seven_inch=c(h,l),this.any=this.apple.device||this.android.device||this.other.device||this.seven_inch},v=i.isMobile=new l;v.Class=l})(window);

        if (isMobile.apple.phone || isMobile.android.phone || isMobile.android.tablet || isMobile.seven_inch) { murphys.shared.mobileOrientation(); }

    },

    preloadImages : function (srcs, imgs, callback) {

        var img;
        var remaining = srcs.length;


        for (var i = 0; i < srcs.length; i++) {

            img = new Image();

            img.onload = function() {

            --remaining;

            value = (1-(remaining/srcs.length)) * 100;
            stringValue = Math.round(value.toString());


            $( "#perc" ).html( stringValue + "%" );
            if ( (remaining <= 0) && (typeof callback === 'function') ) { callback(); }

        };

        img.src = srcs[i];
        imgs.push(img);

        }

    },


    fullPage : function () {

        //fix firefox 48 opacity issue
        var FIREFOX = /Firefox/i.test(navigator.userAgent);
        if(navigator.userAgent.indexOf('Mac') > 0){
          var isMac = true;
          console.log("is mac")
        }

        $('#fullpage').fullpage({

            // you can set the sections background-color below:
            //                   0         1         2           3           4         5          6          7          8          9          10         11
            sectionsColor: ['#69a9ab', '#69a9ab', '#69a9ab', '#94f9f1', '#94f9f1', '#23d9a7', '#19D5A3', '#91d8c5', '#95ef86', '#95ef86', '#95ef86', '#95ef86'],

            keyboardScrolling: false,
            scrollingSpeed: 500,
            scrollOverflow: true,
            controlArrows: false,
            verticalCentered: false,
            allowPageScroll: true,
            resize:false,

            afterLoad: function(anchorLink, index){

                if(index == 2){
                    $(".scene-1-char img").remove();
                    $(".scene-1-char").delay(800).append('<img src="/~/media/OM/murphys-game/ep4/images/animations/sc-1.gif" />');
                    setTimeout(function() {
                        if (FIREFOX) {
                            $('html, body').animate({
                                scrollTop: $(".scene-1-bg-1").offset().top
                            }, 100);
                        }
                    }, 100);

                    $(".intro-bg-1").remove();
                }

                if(index == 3){
                    // if(isMac){
                    //   $('#scene-2 .scene-2-bg-2 [src*="sc-2.gif"]').css('top','-43px');
                    //   if (window.matchMedia("(max-width: 760px)").matches) {
                    //     $('#scene-2 .scene-2-bg-2 [src*="sc-2.gif"]').css('top','-69px');
                    //   }
                    // }
                    // if (FIREFOX) {
                    //   $('#scene-2 .scene-2-bg-2 [src*="sc-2.gif"]').css('top','-40px');
                    // }
                    $(".scene-1-char").remove();
                }

                if(index == 4){
                    // if(isMac){
                    //   $('#scene-3 .scene-3-char').css('top','-70px');
                    //   if (window.matchMedia("(max-width: 760px)").matches) {
                    //     $('#scene-3 .scene-3-char').css('top','-42px');
                    //   }
                    // }
                    // if (FIREFOX) {
                    //   $('#scene-3 .scene-3-char').css('top','-63px');
                    // }
                    $(".scene-2-char").remove();
                }

                if(index == 5){
                    $(".scene-3-char").remove();
                }

                if(index == 6){
                     if (FIREFOX) {
                      $('html, body').animate({
                            scrollTop: $("#scene-5").offset().top
                        }, 2000);
                    }
                   $(".q1-char").remove();
                }

                if(index == 7){
                   $(".scene-5-char").remove();
                }

                if(index == 8){
                   //leave empty for match game
                }

                if(index == 9){
                    $(".scene-7-char").append('<img src="/~/media/OM/murphys-game/ep4/images/animations/sc-7.gif" />');
                    $(".q2-char").remove();
                    $("#gameContainer").remove();
                }
                if(index == 10){
                    // $(".q3-char").append('<img src="images/animations/sc-8.gif" />');
                    $(".scene-7-char").remove();
                }
                if(index == 11){
                    $(".q3-char").remove();
                }

            }

        });

        //if ($('#form').is(":hidden")){ $.fn.fullpage.setMouseWheelScrolling(false); }
        $.fn.fullpage.setAllowScrolling(false);

    },

    buttons : function () {

        $('.js-go-s1').click( function() { $.fn.fullpage.moveTo(2); })
        $('.js-go-s2').click( function() { $.fn.fullpage.moveTo(3); })
        $('.js-go-s3').click( function() { $.fn.fullpage.moveTo(4); })
        $('.js-go-q1').click( function() { $.fn.fullpage.moveTo(5); })
        $('.js-go-s5').click( function() { $.fn.fullpage.moveTo(6); })
        $('.js-go-q2').click( function() { $.fn.fullpage.moveTo(7); })
        $('.js-go-mg').click( function() { $.fn.fullpage.moveTo(8); })
        $('.js-go-s7, #gameSkipBtn, .gameCloseBtn').on('click', function() { $.fn.fullpage.moveTo(9); })
        $('body').on('click', '#gameContinueBtn', function() {
         $.fn.fullpage.moveTo(9);
        });
        $('.js-go-q3').click( function() { $.fn.fullpage.moveTo(10); })
        $('.js-go-s9').click( function() { $.fn.fullpage.moveTo(11); })

        $('.js-go-form').click( function() {
            $.fn.fullpage.moveTo(12);
            // $(".btn-tandc").addClass("dark");
            // function goToForm(){
            //     $("#form").css("display", "inline-block");

            // }

            // goToForm();

        })

        $('.btn-tandc').click( function(){
            console.log('terms clicked');
            $('#tandc').show(); })
        $('#tandc').click( function(){ $('#tandc').hide(); })

    },

    questions : function () {

        var q1a1 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Not a wise choice. Call CAA Travel Insurance instead. They&rsquo;ll ensure you&rsquo;ll get a replacement prescription and reimburse you for up to $200 for hearing aids<sup>2</sup> while you&rsquo;re on your vacation.</p>";
        var q1a2 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! They&rsquo;ll ensure you&rsquo;ll get a replacement prescription and reimburse you for up to $200 for hearing aids<sup>2</sup> while you&rsquo;re on your vacation.</p>";
        var q1a3 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Oh no! Call CAA Travel Insurance instead! They’ll get you a replacement prescription and reimburse you for up to $200 for hearing aids<sup>2</sup> while you’re on vacation.</p>";

        var q2a1 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>That won&rsquo;t help. Call CAA Travel Insurance. They&rsquo;ll help you find a hospital and explain how to properly submit your claim.</p>";
        var q2a2 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Over-the-counter meds won&rsquo;t work. Call CAA Travel Insurance. They&rsquo;ll help you find a hospital and explain how to properly submit your claim.</p>";
        var q2a3 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Exactly! They&rsquo;ll help you find a hospital and explain how to properly submit your claim.</p>";

        var q3a1 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! They&rsquo;ll help you find a vet<sup>3</sup> and explain how to properly submit your claim.</p>";
        var q3a2 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Not the best idea. Call CAA Travel Insurance. They&rsquo;ll help you find a vet<sup>3</sup> and explain how to properly submit your claim.</p>";
        var q3a3 = "<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Nope. Call CAA Travel Insurance. They&rsquo;ll help you find a vet<sup>3</sup> and explain how to properly submit your claim.</p>";


       $( '.cta li' ).click( function () {

        clickedId = eval( this.id );

        if( (clickedId == q1a2) || (clickedId == q2a3) || (clickedId == q3a1) ){

            $( ".answer-container" ).addClass( "correct" );
            $( ".answer-container" ).removeClass( "incorrect" );
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" class\"js-answer-icon\" alt=\"Correct!\"/>")

        } else {

            $( ".answer-container" ).addClass( "incorrect" );
            $( ".answer-container" ).removeClass( "correct" );
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"https://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/>")


        }

        $( ".answer" ).html( clickedId );
        $.fn.fullpage.moveSlideRight();

        })

    },

    formSettings : function () {

        var settings = {

            formId:         "#form",
            lid:            "1515504",
            successPage:    "https://caasco.com/OM/MurphysGame/4/user-return",
            errorPage:      "https://caasco.com/OM/MurphysGame/4/user-return",
            actionMainUrl:  "https://cl.exct.net/subscribe.aspx",
            mid:            "10845500"

        };

        var actionUrl = settings.actionMainUrl + "?lid=" + settings.lid;

        $( "input[name='thx']" ).attr( "value", settings.successPage );
        $( "input[name='err']" ).attr( "value", settings.errorPage );
        $( "input[name='MID']" ).attr( "value", settings.mid );
        $(settings.formId).get(0).setAttribute( 'action', actionUrl );

    },

    formFieldAnimation : function () {

        /*-----
        Shows the form label, in place of the placeholder, when a value has been entered into a field
        -----*/

        $('.field').each(function(){

            var parent = $(this),
                field = parent.find('input, select');

            // Focus: Show label
            field.focus(function(){
              parent.addClass('show-label');
            });

            // Blur: Hide label if no value was entered (go back to placeholder)
            field.blur(function(){
              if (field.val() === '') {
                parent.removeClass('show-label');
              }
            });

            // Handles change without focus/blur action (i.e. form auto-fill)
            field.change(function(){

                if (field.val() !== '') {
                    parent.addClass('show-label');
                } else {
                    parent.removeClass('show-label');
                }

            });

        });

        /*-----
        Extra JS helpers, not directly related to placeholder labels
        -----*/

        // Support placeholders in IE8 via https://github.com/mathiasbynens/jquery-placeholder
        $('input, textarea').placeholder();

        // Add class no-selection class to select elements
        $('select').change(function(){
            var field = $(this);
            if (field.val() === '') {
              field.addClass('no-selection');
            }
            else {
              field.removeClass('no-selection');
            }
        });

    },

    formValidation : function () {

        $('#btn').attr('disabled', 'disabled');

        var isChecked = "n";

        function validateFields(){

            var testEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('#email-address').val();

            if( (!firstName)
                || (!lastName)
                || (!testEmail.test(email))
              ) {
                $('#btn').attr('disabled', 'disabled');
                $('#btn').addClass('disabled');
            } else {
                $('#btn').removeAttr('disabled');
                $('#btn').removeClass('disabled');
            }

        }

        $('form').keyup(function( event ) { validateFields(); });
        $('input').click(function() { validateFields(); });

        $('#check-more').change(function(){

            console.log("clicked");
            isChecked = this.checked ? 'y' : 'n';
            $( "#send-value" ).attr( "value", isChecked );
            console.log(isChecked);

        });

    },

    mobileOrientation : function () {

        if(typeof window.orientation === 'undefined') {

            var test = window.matchMedia("(orientation: landscape)");

            test.addListener(function(m) {

                if ($('#form').is(":hidden")){

                    if(m.matches) {
                        $('#fullpage').hide();
                        $('#warning-rotate').show();
                    } else {
                        $('#fullpage').show();
                        $('#warning-rotate').hide();
                        $.fn.fullpage.reBuild();
                    }
                }

            });

        } else {

            function statusOrientPage(){

                if( window.orientation != 0 ){
                    $('#fullpage').hide();
                    $('#warning-rotate').show();
                } else {
                    $('#fullpage').show();
                    $('#warning-rotate').hide();
                    $.fn.fullpage.reBuild();
                }

            }

            statusOrientPage();
            window.addEventListener("orientationchange", function() { statusOrientPage(); }, false);

        }

    },

    ga : function (){

        //GA
        //..............................................................
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-9277140-1', 'auto');
          ga('send', 'pageview');

          console.log(' GA loaded ');

        // GA tracking events
        //..............................................................
        function gaEvents(argId, argCategory, argAction, argLabel){

            $( argId ).click( function(){
                console.log(argId +" - " + argCategory +" - " + argAction +" - " + argLabel );
                ga('send', 'event', { eventCategory: argCategory, eventAction: argAction, eventLabel: argLabel });
            });

        }

        gaEvents('.js-go-s1.go-next', 'Murphys 4', 'Click', 'Play now');
        gaEvents('#btn.linkButton', 'Murphys 4', 'Download', 'Ebook');
    }

}

$(document).ready(function() {  murphys.shared.init();  });


// ..............................................................................
