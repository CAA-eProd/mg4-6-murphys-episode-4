(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 350,
	height: 140,
	fps: 30,
	color: "#FFFFFF",
	webfonts: {},
	manifest: []
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.sonsvg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#603913").s().p("AgaAUQgFgGgDgJQgIgOAEgNIAZATQAcATAZAHg");
	this.shape.setTransform(31.8,3.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#603913").s().p("AANAGQgagXgYgKIBCALIAGAQQAGAPgGANIgWgWg");
	this.shape_1.setTransform(27.8,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#603913").s().p("AAYAHQgygagqgHIBsgHIAQAWQAPAXgDAWQgSgNgagOg");
	this.shape_2.setTransform(31.5,17.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#603913").s().p("AAYAHQgygagpgHIBsgHIAQAWQAPAXgEAWQgSgNgagOg");
	this.shape_3.setTransform(36.8,17.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#754C29").s().p("AgrADIABgKIBNAKIAJAFg");
	this.shape_4.setTransform(53,26.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#754C29").s().p("AgiACIBOgHIABAJIhYACg");
	this.shape_5.setTransform(32.3,26.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E8C4A1").s().p("AgNAyQgRgGgOgKQgPgKABgFQABgEAWALQAWAKAEgIQADgEghgSQgggSADgGQAFgHAeATQAfAQACgDQABgEgcgTQgdgVAEgGQAIgJAeAYQAcAZACgDQADgDgTgWQgSgWAEgGQAGgHAXAcQAZAbACgBQAEgCgCgRQgCgPALAAQAIAAACAYQACAdACAFQAEAPgGAOIgDAEIgQAPQgEABgGAAQgSAAgfgLg");
	this.shape_6.setTransform(6.9,51.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E8C4A1").s().p("AgwA6IgPgPIgDgFQgFgOAFgPQACgFAEgdQADgYAJABQAKABgCAPQgDARADACQABABAbgbQAZgZAFAHQAEAFgVAVQgTAVADADQACADAegXQAfgWAHAKQAEAGgeATQgdASABAEQACADAfgPQAfgRAEAHQAEAHgiAPQghASACADQAFAIAWgJQAWgIAAADQACAFgPAJQgQAKgQAEQgdAJgRAAQgJAAgFgCg");
	this.shape_7.setTransform(79,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#603913").s().p("ADCCaIgBgxQgCgvgWghQgvhBh6ABQhyAAgzA5QgeAkgCAzQgBAtgIACQgEABgEgIQgEgagKgLIgKgFQgQgLAHgiIAKghQAGgWALgbQAIgSACgEQAGgGAEgHQAKgZAVgQQAKgJATgLQAagOAJgGQASgMAjgFQAhgEAmACQAgACBPA+QAoAfAhAeIARAbQAIAQAFARIAMAdQAKAggLAKQgTATgLATQgLAagFAAQgDAAgBgHg");
	this.shape_8.setTransform(42,19);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3366B0").s().p("AAqAWQgDhFgnAFQgYADgLBBQgEAhgBAhIgWgIIghisIAAgGIC/AAIgdCtIgeAQQAHglgCgkg");
	this.shape_9.setTransform(42.4,87.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B2987E").s().p("AAFARQgMgCgJgXQgGgOARAKQANAJAIAHQAEAEgCAFQgEAEgGAAIgDAAg");
	this.shape_10.setTransform(18,32.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E8C4A1").s().p("AAFAiQgcgcgFgTQgEgUATgHQANgGAMAKQAIAFAFAGIAFArQAAAZgKAAQgGAAgJgJg");
	this.shape_11.setTransform(16.8,33.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#B2987E").s().p("AgRAOQgCgGAEgDQAIgIANgJQARgKgGAPQgJAXgMABg");
	this.shape_12.setTransform(65.3,32.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E8C4A1").s().p("AgdASIAFgrIANgLQANgKANAGQATAHgFAUQgFATgcAcQgJAJgGAAQgKAAAAgZg");
	this.shape_13.setTransform(66.4,33.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3B5053").s().p("AgjAcQgPAAgFgEIgCgEQgEgJACgJQADgIAHgGIAUgMQAZgJATAMIAsAYQAEADgKAKQgLALgfACIgugBg");
	this.shape_14.setTransform(51.2,98.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3B5053").s().p("AgKAYQgdgDgPgIQgIgEALgJQANgMAagIQANgGARABQAQACAOAJQAHAGACAIQADAJgEAIIgCAFQgFAEgPAAIgsgCg");
	this.shape_15.setTransform(34,98.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#5687C5").s().p("AhmB2QAJhQgFhSIhVARIgCg1IBRgQQAngOAlgHIACACIgBgCIAGAGIAGADIAFACIABAAIADABIAGAAIAAABIAAgBIAGAAIAEgBIABAAIAFgCIAFgDIAGgEIABgCQAnAHAmAOIBRAQIgCA1IhcgRQgCBQAHBSg");
	this.shape_16.setTransform(42.7,65.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E8C4A1").s().p("AAKAiQgzgEg3gOQgEgDAAgGQAAgEACgDQADgBAFABQAwAKAwAEQAWABAVgPIAkgdQAGgHAHAHQAHAHgHAGQghAdgGAEQgXARgWAAIgEAAg");
	this.shape_17.setTransform(65.5,58.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E8C4A1").s().p("Ag8APIgmggQgEgFAEgFQADgEADgBQAEAAAEACQAaAXAMAHQAUARAZgEQAygGApgIQAGgBADAEQACADgBAEQgBAEgEACQg3AQg2ACQgWAAgYgSg");
	this.shape_18.setTransform(21.1,58.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E8C4A1").s().p("Ag+B9IgUiLQgGgiAAgcIACgwIAPACQAXABAvAAQBJgBARgCQgBAlgCAOQgBAOgJAtIgUCKIgTAAQAChHgDg1QgBgOgOgEIgPgBIgIgDIgKADQgHgBgHACQgPAEgBAOQgDA2ACBHg");
	this.shape_19.setTransform(42.7,84.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E8C4A1").s().p("AgRAgQgIgJAAgKIAAgYQAAgLAIgIQAHgHAKgBQALABAHAHQAIAIAAALIAAAYQAAALgIAIQgIAHgKAAQgKAAgHgHg");
	this.shape_20.setTransform(42.5,54.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#3C2415").s().p("Ag/gVIA/ACQApAAAWgCQgOAqgxABIAAAAQgwAAgPgrg");
	this.shape_21.setTransform(42.1,45.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#534132").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_22.setTransform(33.1,33.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#534132").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_23.setTransform(51,33.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgIAHgIQAIgHAIAAQAJAAAIAHQAHAIAAAIQAAAKgHAHQgIAHgJAAQgIAAgIgHg");
	this.shape_24.setTransform(50.8,33.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgQARQgHgHAAgKQAAgIAHgIQAIgHAIAAQAJAAAIAHQAHAIAAAIQAAAKgHAHQgIAHgJAAQgIAAgIgHg");
	this.shape_25.setTransform(33.2,33.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#CFAF8B").s().p("AABATQgLAAgIgGQgJgHAAgGQAAgMAMgGQgDADAAAGQAAAIAJAFQAIAGALAAQAIAAAKgEQgKANgOAAIgDAAg");
	this.shape_26.setTransform(41.8,39.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E8C4A1").s().p("AimCnQhGhFAAhiQAAhhBGhFQBFhGBhAAQBiAABFBGQBGBFAABhQAABihGBFQhFBGhiAAQhhAAhFhGg");
	this.shape_27.setTransform(41.8,29.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#603913").s().p("Ai5C6QhOhNAAhtQAAhrBOhOQBNhOBsAAQBtAABNBOQBOBOAABrQAABthOBNQhNBOhtAAQhsAAhNhOg");
	this.shape_28.setTransform(41.8,26.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,86.1,101);


(lib.sealarm = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2D4051").s().p("AniCjQAAgjAehDQAhhKAwg4QCEidCIBrQB4BgEDBSQB1AmAeAOQA7AZABAcg");
	this.shape.setTransform(-27,1.6,0.693,0.737,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.5,-10.5,67,24.2);


(lib.momeyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#534132").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape.setTransform(16.9,2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534132").s().p("AgNAOQgGgGAAgIQAAgHAGgGQAGgGAHAAQAIAAAGAGQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGg");
	this.shape_1.setTransform(2,2.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,18.9,4.1);


(lib.grampshand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8C4A1").s().p("Ag/A6IgCACQACgMgFgMQgGgPgKgEQgBgHABgGQABgiARgSQAUgTATAJQATAIgGAVIAXgMQAfgKAYANQAgATgPAiQgQApg6AEIgiACQgZAAgLgEg");
	this.shape.setTransform(8.6,6.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,17.3,12.5);


(lib.daughtersvg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED1652").s().p("Ag6BKIgBhHQAAgbADgDIgMAPIgUgUIAUgaQAXgJAbgGIACABIgBAAIAHAGIABAAIADABIAOAAIADgCIABAAIADgBIAEgFQAcAGAXAJIATAJIgGAZIgVgFIAABng");
	this.shape.setTransform(25.1,47.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#008D72").s().p("AgwA7IgLh0IAAgBIB3AAIgKB0IgtAAIAAgaQAAgFgFgDQgDgCgEACQgDABgCAEQgEAPAAAPg");
	this.shape_1.setTransform(25.3,58.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_2.setTransform(15.5,31.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_3.setTransform(17.7,31.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_4.setTransform(15,29.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_5.setTransform(32.6,32.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_6.setTransform(34.7,31.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E6AA96").s().p("AgEAAQAAgEAEAAQAFAAAAAEQAAAFgFAAQgEAAAAgFg");
	this.shape_7.setTransform(32.6,30.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#534132").s().p("AgJAKQgFgEAAgGQAAgFAFgEQAEgFAFAAQAGAAAEAFQAFAEAAAFQAAAGgFAEQgEAEgGABQgFgBgEgEg");
	this.shape_8.setTransform(18.3,26.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgLANQgGgGABgHQgBgGAGgFQAFgFAGgBQAHABAFAFQAGAFAAAGQAAAHgGAGQgFAEgHAAQgGAAgFgEg");
	this.shape_9.setTransform(18.3,26.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#534132").s().p("AgJAKQgFgEAAgGQAAgFAFgEQAEgFAFAAQAGAAAEAFQAFAEAAAFQAAAGgFAEQgEAEgGABQgFgBgEgEg");
	this.shape_10.setTransform(30.7,26.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgLANQgFgGgBgHQABgGAFgFQAFgFAGgBQAHABAFAFQAGAFgBAGQABAHgGAGQgFAEgHAAQgGAAgFgEg");
	this.shape_11.setTransform(30.6,26.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E8C4A1").s().p("AgTAlQgBgBANgEQAMgFgBgGQAAgDgUAFQgWAFgBgEQgBgFAVgEQATgEAAgDQAAgDgVADQgVADAAgEQgBgHAXAAQAVgCAAgCQAAgCgPgCQgSgCAAgEQgBgFAXABQAUACAAgBQABgCgIgHQgFgGACgFQAEgEALAKQAOAOACABQAKAFACAJIAAACIgBAMQgFAJgaAMQgJAEgJACIgGAAQgFAAgBgCg");
	this.shape_12.setTransform(4.4,50.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E8C4A1").s().p("AgdAjIgJgJIgBgDQgDgJADgJIADgTQACgPAFABQAGABgBAJQgCAKACABIARgPQAOgQADAEQADAEgNAMQgLANACACQACABAQgNQATgOAEAGQADAEgSAMQgRAKAAACQABACATgIQATgLACAEQADAEgVAJQgUAKABADQADAFAOgGQANgFAAACQABADgJAGQgJAFgKADQgQAFgKAAQgGAAgEgBg");
	this.shape_13.setTransform(49.7,39.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3C2415").s().p("AgHALQgFgEABgKIACgLIANAAIAAAFIgJAAIABAQQACAFAEgIQAFgIADgBQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIgKATg");
	this.shape_14.setTransform(27.1,70.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3C2415").s().p("AgCAIIgBgPQADgBAEACQgBAGgDAIg");
	this.shape_15.setTransform(31.4,70.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#3C2415").s().p("AgeAJQgBgBAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAAAAIgCgMIAJAIIA5AAQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAAgBABQAAAAAAABg");
	this.shape_16.setTransform(29.3,71.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E8C4A1").s().p("AgIASQgQABgGgHIgCgHIABgLIAAAAQADgGAEgEQAGgHAAAQQgBAHAaAAQAcAAgCALQgCAHgNABIgagBg");
	this.shape_17.setTransform(29.4,69.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#3C2415").s().p("AgLgEIACgBQADABAFAIQAEAIACgFQABgJAAgHIgJAAIAAgFIANAAIACALQABAKgFAEIgJAEg");
	this.shape_18.setTransform(22.3,70.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#3C2415").s().p("AgCgGQACgCADABIgDAPIAAAAIgCgOg");
	this.shape_19.setTransform(17.8,70.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3C2415").s().p("AggAJQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQABgBAAAAIA5AAIAJgIIgCAMQAAAAAAABQAAAAAAABQAAAAAAABQAAAAgBABg");
	this.shape_20.setTransform(20.1,71.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#B2987E").s().p("AAHAHQgHgIgLgFQgCgHALAGQAHAGAGAEQADAEgDACIgCACg");
	this.shape_21.setTransform(9.5,26.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E8C4A1").s().p("AADAXQgRgTgEgMQgDgNAMgFQAJgEAIAHQAFADADAEIADAcQAAARgGAAQgEAAgGgGg");
	this.shape_22.setTransform(8.8,26.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#B2987E").s().p("AgLAJQgBgEADgCQAEgEAIgGQALgHgCAIQgLAFgHAIQAAAAAAABQgBAAAAABQAAAAAAABQAAAAAAABg");
	this.shape_23.setTransform(39.6,26.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E8C4A1").s().p("AgSAMIADgcIAIgHQAIgHAJAEQAMAFgDANQgEAMgRATQgGAGgEAAQgGAAAAgRg");
	this.shape_24.setTransform(40.4,26.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#603913").s().p("AiQChQgVgLgFgRQgFgPgHgmQgJgtABgTQAAgMAFgKIAEgNQASg3AMgUQAVghAlgQQA8gaAnACQAlACAHAXQArgIA0AzQAhAjAHAvQAHAOgCAQQgFAygNAyIgBAEQgNAbgaARQAIgXAEgdQgBgPACgPIgEgLIgGAMQgBAJgGALQgGgQAEgnQgFANgLAJQgJgbgDghIgDAEIAAAFIgBgDIgaAfQgQgZgKgUQgIgUABgWQgKARgQASQgWAXgEANIgOguQgNAjgZAgQgXgQgCgPIgCABQgLA0gJANIgTgkIgFAtIgCANIABAXQgBARARAWQgJgCgKgFg");
	this.shape_25.setTransform(24.7,16.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#754C29").s().p("AgsADIACgKIBNAJIAJAGg");
	this.shape_26.setTransform(32.2,20.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#754C29").s().p("AghACIBNgIIABALIhYACg");
	this.shape_27.setTransform(16.6,21);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E8C4A1").s().p("AggALQgBgLAbAAQAaAAgBgHQAAgQAHAHQADAEAEAGIAAALIgCAHQgGAHgQgBIgaABQgNgBgCgHg");
	this.shape_28.setTransform(20,69.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E8C4A1").s().p("AANBTQACgvgCgkQgBgIgKgDIgCgBIgBABQgJACgBAJQgCAkABAvIgMAAIgOhcQgGgkADgjQgMgBAMgBIAAACIAdAAQAwAAAMgCQgCAngGAiIgOBcg");
	this.shape_29.setTransform(24.7,60.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E8C4A1").s().p("AAHAWQghgCglgJQgFgCABgGQABgBAAAAQAAgBAAAAQABgBAAAAQAAAAABAAQACAAADAAQAeAGAhADQAPABAOgKQADAAAUgTQAFgEAEAEQAFAFgFAEIgaAVQgPALgPAAIgCAAg");
	this.shape_30.setTransform(40.3,43.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E8C4A1").s().p("Ag6AiQABgHAGAAIAigBQAQAAAJgPQARgRAWgfQADgDAFACQAFADgDAFQgUAfgaAbQgKAJgSADIgiAAQgGAAgBgGg");
	this.shape_31.setTransform(13.2,47);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E8C4A1").s().p("AgLAVQgFgGAAgGIAAgQQAAgHAFgGQAFgFAGAAQAGAAAGAFQAFAGAAAHIAAAQQAAAGgFAGQgFAFgHAAQgGAAgFgFg");
	this.shape_32.setTransform(25.1,40.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#3C2415").s().p("AgpgNIApABIAqgBQgJAbghAAIAAAAQgfAAgKgbg");
	this.shape_33.setTransform(24.8,34.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CFAF8B").s().p("AAAAKQgGABgFgEQgGgEgBgFQAAgEACgEQACAHAFADQAGAEAFgBQALABAGgGQgBAFgGAEQgFAEgFAAIgCgBg");
	this.shape_34.setTransform(24.6,30.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E8C4A1").s().p("AhuBvQguguAAhBQAAhAAuguQAuguBAAAQBBAAAuAuQAuAuAABAQAABBguAuQguAuhBAAQhAAAgugug");
	this.shape_35.setTransform(24.6,24);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#603913").s().p("AgiCZQgCgIAAgRQgIAGgHAkQgDATgCARQgWgggEglQgDAXgGAaQgTgagFgOQgHgPADgUQgMAYgRAQQgciNgCgxQgHiKgBgnIF1AAIgICsQgDAxgbCSQgHgDgIgIQgPgOgBgUQgDApgRAZIgWAWQAAghgGghQgEATggA5QgNg5gBgRQgEATgIAeQgIAhgEAFQgYgkgGgcg");
	this.shape_36.setTransform(24.8,42.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,53.9,72.2);


(lib.dadeyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#534132").s().p("AgOAPQgGgGAAgJQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHQAAAJgGAGQgHAGgIAAQgHAAgHgGg");
	this.shape.setTransform(17.7,2.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534132").s().p("AgNAPQgHgGAAgJQAAgHAHgHQAFgGAIAAQAIAAAGAGQAHAHAAAHQAAAJgHAGQgGAGgIAAQgIAAgFgGg");
	this.shape_1.setTransform(2.2,2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,19.8,4.3);


(lib.babytorso = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#854E9F").s().p("AgfBmIgTgGQgVgOgFgoQgEgdAGgiIAKhQIABgBIAEgFIAFACQAcAGAbACQAeABAVgGIAEAGIABAAIAMA7QACARAHAkQADAogTAWQgJAMgQAJQgRAJgRAAQgMAAgWgGg");
	this.shape.setTransform(7.9,10.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,15.9,21.7);


(lib.babyeyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#534132").s().p("AgEAOQgHgCgCgGQgDgGADgFQACgGAGgCQAFgDAGADQAGACACAGQADAFgDAFQgCAHgGACIgGABIgEgBg");
	this.shape.setTransform(13,1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534132").s().p("AgFAOQgGgCgCgHQgDgFADgFQACgGAGgCQAFgDAFACQAHADACAGQADAFgDAFQgCAGgGADQgEACgCAAIgFgCg");
	this.shape_1.setTransform(1.6,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,14.6,3.9);


(lib.babyarmR = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8C4A1").s().p("AgDAYIgBAAQgOgGgEgFQgDgEACgIQAHgMADgEQAKgLALADQAOADACALQABAGgDAHQgIAVgOAAIgDgBg");
	this.shape.setTransform(-2.3,2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#854E9F").s().p("AgFAjQgjgGgIgMQASAEAJgWQAEgJgBgHQANAMAWgNQAPgHAJgJIAAgBIAAgBIADABQABADACAMQACAOgBAJQgCAJgFAIQgEAGgCACQgGAFgEACQgFACgJAAIgQgCg");
	this.shape_1.setTransform(2.2,2.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.8,-0.8,11.9,7.5);


(lib.babyarmL = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#854E9F").s().p("AgsAjQgVgSACgOQADgLAFgJQAMAJAigIQAmgHALgMIAAAAQACAOAEAHIADADQAHAIAIABQgNAHglAQQgUAKgOAEIgEABg");
	this.shape.setTransform(6.7,5.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8C4A1").s().p("AAAAZQgGgBgHgIIgDgDQgEgGgCgMIAAgDQgBgKAQgEQAOgFALAQIABACQALAPgPANQgHAGgHAAIgBAAg");
	this.shape_1.setTransform(13.1,2.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.2,0,15.3,9.1);


(lib.seal = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjjG6IgCgeIAAouQAAgjAJgkQAdh3Bog/QBng+B2AdQAuCGAZCJQAZCJAABtQAADtiFBJQg3AfhYAKQg4AGhtAAIgQAAg");
	this.shape.setTransform(65.6,71.6,0.693,0.737,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgcAdQgNgMAAgRQAAgQANgNQAMgMAQAAQARAAAMAMQANANAAAQQAAARgNAMQgMANgRAAQgQAAgMgNg");
	this.shape_1.setTransform(77.4,9.2,0.693,0.737,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ABwAuIjihSQgEgCABgEQACgFAFACIDhBSQAEACgCAFQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAgBAAg");
	this.shape_2.setTransform(59.8,11.7,0.693,0.737,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ah1AqQAAgEADgBIDihSQAEgCACAFQABAEgEACIjhBSIgCABQgFAAAAgFg");
	this.shape_3.setTransform(59.8,22.5,0.693,0.737,0,0,180);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ah4AEQgFAAAAgEQAAgEAFAAIDxAAQAFAAAAAEQAAAEgFAAg");
	this.shape_4.setTransform(60.8,17.1,0.693,0.737,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2D4051").s().p("AgbAkQgRgcAIgeQAIggAcgRQAnApAAAqQgBAhgTAbQgegIgQgcg");
	this.shape_5.setTransform(46.1,17.7,0.693,0.737,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#517391").s().p("AgIIJIABgXIAAmdQgBhYg8hAQg7g/hUAAQgbAAgWAGQAIgeAPgmQhpgqgig3QAWgGALgVQAMgUgGgYQgGgYgTgLQAvg0BoglQBhgkBQAAIAkADIAcADQB0gBBpDSQBcC4AjDmQgcgng9gEQhBgDgrAoQgbAYgNAXQgQAbAAAfIAAE5g");
	this.shape_6.setTransform(82.5,52.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2D4051").s().p("AhRD9QhthOAAjXQABhiAQhnQAegjApgTQApgTAuAAQBUAAA9A9QA8A9ABBWIAAGlQjAgEhQg6g");
	this.shape_7.setTransform(108.2,80.9,0.693,0.737,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#2D4051").s().p("AmnBvQAfgOB1gmQEChSB4hgQCJhrCECdQAwA4AhBKQAeBDAAAjIvFABQABgcA6gZg");
	this.shape_8.setTransform(128.5,92.1,0.693,0.737,0,0,180);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#517391").s().p("AqEFYQABgyBtg/QAugaDVhiQDChaBuhEQCnhpBUhyQBPhsBKAzQA9ArA2CTQArB2AdCdQAZCIAABFQjrABkhAAg");
	this.shape_9.setTransform(139.7,78.8,0.693,0.737,0,0,180);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#2D4051").s().p("AjXAiQAng2A2gpICgByIg8itQBFgdBNgEQBNgDBJAWIhZEdInLADQAUhAAng4g");
	this.shape_10.setTransform(193,93,0.693,0.737,0,0,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(60));

	// Layer 3
	this.instance = new lib.sealarm();
	this.instance.setTransform(98.9,102.9,1,1,0,0,0,33.5,12.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AjjG6IgCgeIAAouQAAgjAJgkQAdh3Bog/QBng+B2AdQAuCGAZCJQAZCJAABtQAADtiFBJQg3AfhYAKQg4AGhtAAIgQAAg");
	this.shape_11.setTransform(65.6,71.6,0.693,0.737,0,0,180);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgcAdQgNgMAAgRQAAgQANgNQAMgMAQAAQARAAAMAMQANANAAAQQAAARgNAMQgMANgRAAQgQAAgMgNg");
	this.shape_12.setTransform(77.4,9.2,0.693,0.737,0,0,180);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("ABwAuIjihSQgEgCABgEQACgFAFACIDhBSQAEACgCAFQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAgBAAg");
	this.shape_13.setTransform(59.8,11.7,0.693,0.737,0,0,180);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("Ah1AqQAAgEADgBIDihSQAEgCACAFQABAEgEACIjhBSIgCABQgFAAAAgFg");
	this.shape_14.setTransform(59.8,22.5,0.693,0.737,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ah4AEQgFAAAAgEQAAgEAFAAIDxAAQAFAAAAAEQAAAEgFAAg");
	this.shape_15.setTransform(60.8,17.1,0.693,0.737,0,0,180);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#2D4051").s().p("AgbAkQgRgcAIgeQAIggAcgRQAnApAAAqQgBAhgTAbQgegIgQgcg");
	this.shape_16.setTransform(46.1,17.7,0.693,0.737,0,0,180);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#517391").s().p("AizLCIAAmnQAAgtgTgqQgTgpgjgeQhBg4hWAIQhVAGg4BCQAxk5CGj7QCXkcCnAAIAogCQAmgEARAAQBzAACMAwQCWAyBDBGQgcARgIAfQgIAgARAcQAQAdAgAHQgyBMiXA5QAVAzANAoQghgIgmAAQh6AAhWBWQhWBXAAB4IAAIvQAAAQABAPg");
	this.shape_17.setTransform(82.5,52.1,0.693,0.737,0,0,180);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2D4051").s().p("AhRD9QhthOAAjXQABhiAQhnQAegjApgTQApgTAuAAQBUAAA9A9QA8A9ABBWIAAGlQjAgEhQg6g");
	this.shape_18.setTransform(108.2,81,0.693,0.737,0,0,180);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2D4051").s().p("AmnBvQAfgOB1gmQEChSB4hgQCJhrCECdQAwA4AhBKQAeBDAAAjIvFABQABgcA6gZg");
	this.shape_19.setTransform(128.5,92.1,0.693,0.737,0,0,180);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#517391").s().p("AqEFYQABgyBtg/QAugaDVhiQDChaBuhEQCnhpBUhyQBPhsBKAzQA9ArA2CTQArB2AdCdQAZCIAABFQjrABkhAAg");
	this.shape_20.setTransform(139.7,78.8,0.693,0.737,0,0,180);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#2D4051").s().p("AjXAiQAng2A2gpICgByIg8itQBFgdBNgEQBNgDBJAWIhZEdInLADQAUhAAng4g");
	this.shape_21.setTransform(193,93,0.693,0.737,0,0,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.instance}]}).to({state:[{t:this.instance}]},29).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},8).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({rotation:30},6,cjs.Ease.get(1)).to({regY:12.2,rotation:15},5,cjs.Ease.get(-1)).to({regY:12.1,rotation:30},5,cjs.Ease.get(0.99)).to({rotation:0},8,cjs.Ease.get(1)).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(4.9,0,207.2,104.5);


(lib.momsvg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.momeyes("synched",0);
	this.instance.setTransform(29.3,28.5,1,1,0,0,0,9.5,2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({startPosition:0},0).to({x:28.9},10).wait(126));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#534132").s().p("AhAgWIAPAMQAVAKAbAAQAlABAdgXQgOArgzABIAAABQgxgBgPgsg");
	this.shape.setTransform(29.8,41);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F2B63F").s().p("AAACdQgngBgrgWIgkgWQALgwAGhCQAFhFADgMIANgyQAEgNAAgKQAtAUAhAAQAhABAqgRQACAQAHAZIALAqQAEATAEA9QAEA/AKArQgOALgWAKQgrATgmAAIgCAAg");
	this.shape_1.setTransform(29.2,83.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFD683").s().p("AAABsQgngBgjgZIgBgUQgFg3gZgfIgYgWIAOgSQAPgTAGgDQAcgNAfgDIABAWQACAXAEAIQAMAVAVAJQAegNAFgSQAFgOAAgXIgCgUQAWADAbAHQANAEAOAYQAHAMAEAMQgaASgNAbQgOAdADAgIAAAaQgmAVgnAAIgDAAg");
	this.shape_2.setTransform(29,59.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E8C4A1").s().p("AgSA0QgFgCAKgYQAKgagEAAQgCAAgMAWQgMAYgFgDQgGgDAOgZQANgXgDgBQgHgDgKANQgJAOgBgBQgDgCAFgMQAFgLAIgKQAWgbALgEQACAAANADIAEACQAKAGADAMQAAADAMAVQAIAOgGADQgGAEgFgKQgGgKgDAAQgBAAgFAaQgFAbgGgCQgFgBAEgWQAEgWgDgBQgDAAgHAbQgJAZgIAAIgBgBg");
	this.shape_3.setTransform(5,82.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E8C4A1").s().p("AhDCSQgOgHABgEQABgEAag5IAihNIABgCQAKgYApg+IApg6IAHAvIgjAwQgbAmgJAQIAAABQgJAXg0B8QAAABAAAAQAAAAgBAAQAAABgBAAQAAAAgBAAQgEAAgJgEg");
	this.shape_4.setTransform(12,65.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E8C4A1").s().p("AACAcQgHgbgDAAQgEABAEAWQAEAWgFABQgGACgFgbQgFgagCAAQgDAAgFAKQgFAKgHgEQgGgDAJgOQALgVABgDQADgMAJgGIAEgCQAOgDACAAQALAEAWAbQAIAKAGALQAFAMgDACQgCACgJgPQgKgNgHADQgDABANAXQAOAZgGADQgFADgMgYQgMgWgCAAQgEAAALAaQAKAYgFACIgCABQgIAAgIgZg");
	this.shape_5.setTransform(53.3,82.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E8C4A1").s().p("AAzCUIg8iTIAAgBQgJgQgbgmIgjgwIAHgvIAoA6QApA+ALAXIAAACQAMAcAyBuQABAFgPAHQgIAEgEAAQgBAAgBAAQAAAAgBgBQAAAAAAAAQgBAAAAgBg");
	this.shape_6.setTransform(46.3,65.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#603913").s().p("AgrABIACgOIBMATIAJAJg");
	this.shape_7.setTransform(37.5,24.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#603913").s().p("AgiAFIBMgRIACAOIhXALg");
	this.shape_8.setTransform(20.4,23.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#603913").s().p("AgUA0QgMghABgXQACgVATgcQAKgPAHgKQAAgHAOAfQANAfgDAXQgDAggjAwQgHgLgGgRg");
	this.shape_9.setTransform(18.2,11.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#603913").s().p("AAHA4QgYgXgLgXQgIgRADgiQABgSADgNQgBgFAZAUQAYAUAKAXQAIARgCAmQgBATgCAOQgLgHgOgLg");
	this.shape_10.setTransform(22.4,13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B2987E").s().p("AgQANQgDgFAEgEQAHgGAMgJQAPgKgBAKQgPAHgJALQgFAEADAFIACABQgGgBgEgDg");
	this.shape_11.setTransform(49.1,27.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E8C4A1").s().p("AgcASIAFgqIAMgLQANgKAMAGQATAHgFAUQgFARgbAcQgJAJgFAAQgKAAAAgYg");
	this.shape_12.setTransform(49.3,28.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#603913").s().p("ABYBzQiRgpgphyQgEAmgRAmQgiBSg/ANQgIgQgCgdQgDg5Agg5QAbgxAugNQAJgCAOgCQAEAAgHgCIALgJQAOgLASgGQA7gVBPAaQBYAdAoBzQATA6ADAvIgRABQg6AAhAgSg");
	this.shape_13.setTransform(29.3,13.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#906B29").s().p("AAHAjIgggUQgKgCAAALQAAAFACAGIgKAAIgGgsQgBgOAEgLIAWAAIAAAGIgQAAQABAMAEALQAHAJAKAKIAXAPIgDgfQAOAFAPABIASAbQAAABABABQAAAAAAABQAAAAAAABQgBAAAAAAg");
	this.shape_14.setTransform(36.1,105);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E8C4A1").s().p("AgJAeQgVgLgLgNIgHgJIABgSQACgJAEgFQAHgKAHAVQAKAUASAHIAfAMQARAGAAAHQAAALgXAAIgCAAQgUAAgNgJg");
	this.shape_15.setTransform(36.3,104.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#906B29").s().p("AAiAjQAFgUgLgBIgiAVIgrAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAAAgBIAAgBIAMgPQAMgPABAAQAJgBANgDIgDAgIAYgRQAKgKAGgJQAEgKABgNIgQAAIAAgFIAXAAQAEAMgBAMIgGAtg");
	this.shape_16.setTransform(23,105);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#B2987E").s().p("AAHARQADgFgEgEQgIgKgOgKQgBgKAPALQALAKAFAHQAFAEgEAFQgDADgHAAg");
	this.shape_17.setTransform(9.2,27.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E8C4A1").s().p("AAFAhQgbgcgFgRQgFgUATgHQAMgGANAKQAHAFAFAGIAFAqQAAAYgKAAQgFAAgJgJg");
	this.shape_18.setTransform(8.9,28.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E8C4A1").s().p("AgaAnQgWAAAAgLQAAgGARgGIAfgNQASgHAKgUQAHgVAIAKQADAFACAJIABASIgHAJQgLANgVALQgNAJgUAAIgDAAg");
	this.shape_19.setTransform(22.7,104.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E8C4A1").s().p("AAQCiIgNjJQgDhDgJBDQgIAzgFCWIgSAAIgJhmQgKhogBgKQgGgiAAgbIABguIAAgBICEAAQgDA4gJA0IgVDYg");
	this.shape_20.setTransform(29.8,85.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CFAF8B").s().p("AgQAHQgIgFABgFQAAgFADgEQABAJAHADQAIAFAGgBQAPAAAHgJIAAACQAAAFgHAFQgIAGgJAAQgJAAgHgGg");
	this.shape_21.setTransform(29.2,34.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgQATQgIgIAAgLQAAgKAIgIQAHgIAJAAQAKAAAHAIQAIAIAAAKQAAALgIAIQgHAIgKAAQgJAAgHgIg");
	this.shape_22.setTransform(36.7,28.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgQATQgIgIAAgLQAAgKAIgIQAHgIAJAAQAKAAAIAIQAHAIAAAKQAAALgHAIQgIAIgKAAQgJAAgHgIg");
	this.shape_23.setTransform(21.8,28.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E8C4A1").s().p("Ag0DWQgigRgYgYQglgjgSgjQgYguAAg5QAAhUA3hHQA6hLBMAAQArAABGBIQBMBQABBOQABBqhYBNQgcAZgVAKQgaANgcAAQgSAAgigRg");
	this.shape_24.setTransform(29.2,24.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.lf(["#E8C4A1","#CFAF93"],[0,1],0.1,-5.7,-0.1,-1.2).s().p("AguAYQgegqAKgCQAUgGAHgDQAMgFAAgFQAAgLAIgIQAHgIALABQAJAAAHAIQAIAHAAALQAAAGAOAFIAeALQAKAEglAnQglApgJAAQgLAAgdgrg");
	this.shape_25.setTransform(30.1,51.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#603913").s().p("AAKEDQhRAAhWgiIhHghQgFgsAAg9QABh3AdhSQAdhUA6gfQANgIAQgEQAHgCgHgBQDVhCBLDzQAmB4gGCPQhlA/h2AAIgEAAg");
	this.shape_26.setTransform(28.7,25.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(170));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,58.4,108.6);


(lib.grandpasvg = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// grandpa left arm
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F5EF79").s().p("AjqD5IAOlmIACgrQAEgdgMgaQgLgXgWgOIgHgEQA0AJAlAMIASAEQATAGAEAEIBaBQIAVAMQAZAOAdAJIAEABQAYAIAYADQBAAJBZgRIAMACIACABQAKAFAFAOQAEALgBANQAAALgFAKQhvA0h4gZIgXgFQgwgOgxgpIglggQgbDUALCBg");
	this.shape.setTransform(102,95.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F5EF79").s().p("AjrD4IAOllIACgrQAEgdgNgaQgLgXgVgOIgHgEQAzAJAlAMIATAEQATAGADAEIBcBQIAUALQAaAOAcAJIADABQAZAFAXAAQBAACBXgbIAMABIACABQAKAEAHANQAGALABANQABAKgEALQhuBHh5gRIgYgFQgwgMg0grIgmghQgbDVALCBg");
	this.shape_1.setTransform(102.1,95.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F5EF79").s().p("AjrD4IAOllIACgrQAEgdgNgaQgLgXgVgOIgHgEQAzAJAlAMIATAEQATAGADAEIBcBPIAUAMQAaANAdAJIABABQAZAEAXgFQBAgFBTgkIAMgBIACABQAKADAJANQAHAKACAMQACALgDAKQhqBah5gKIgYgDQgwgKg4guIgnggQgbDVALCBg");
	this.shape_2.setTransform(102.1,95.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F5EF79").s().p("AjqD4IAPlmIABgrQAEgdgNgaQgKgWgWgOIgGgDQAzAIAlALIASAFQATAGADADIBcBPIAVAMQAZANAeAIQgBABABAAQAZADAVgIQBAgMBOgtIALgDIACABQAMACAKAMQAHAJAEAMQADAKgCALQhlBth5gEIgXgCQgxgHg7gwIgoggQgaDUAKCBg");
	this.shape_3.setTransform(102,95.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F5EF79").s().p("AjoD4IAPlmIABgrQAFgdgNgaQgLgWgWgOIgHgDQA0AIAlALIASAFQATAGADADIBdBOIAUAMQAZANAeAIQAAABAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQAZABATgMQA+gSBJg1IAMgDIACAAQALAAALALQAJAIAEAMQAFAJgBALQhdB/h4ACIgXgBQgwgDhBgzIgngfQgbDUALCBg");
	this.shape_4.setTransform(101.7,95.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F5EF79").s().p("AjkD4IAPlmIABgrQAEgdgNgZQgLgXgWgOIgGgDQAzAIAmALIASAFQATAFADAEIBdBOIAUALQAZANAeAIIgBABQAYAAAQgQQA8gZBDg8IALgFIADAAQAKgBAMAJQAKAIAGALQAFAJABALQhUCPh2AIIgXABQgvgBhFg0IgpgeQgaDUAKCBg");
	this.shape_5.setTransform(101.4,95.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F5EF79").s().p("AjfD4IAOlmIACgrQAEgdgNgZQgMgXgVgOIgGgDQAzAIAlALIASAFQAUAFADADIBdBOIAUALQAZANAeAIIgDACQAYgCAPgTQA4ggA8hDIAKgGIADAAQAKgCAOAIQAKAHAHAKQAHAJABAKQhICeh0APIgXACQguABhKg0IgpgeQgbDUALCBg");
	this.shape_6.setTransform(100.9,95.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F5EF79").s().p("AjZD4IAOllIACgrQAEgdgNgaQgMgXgVgNIgGgEQAzAJAlALIASAEQATAFAEAEIBdBNIAVALQAYANAfAIIgEABQAXgCALgXQA1gkA1hKIAJgHIACAAQALgEAOAHQALAFAIAKQAHAHADALQg7CrhxAVIgWAEQgwAEhMg1IgqgeQgbDVALCBg");
	this.shape_7.setTransform(100.3,95.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F5EF79").s().p("AjWD9IAPllIABgrQAEgdgNgZQgMgXgVgOIgGgDQAyAIAmALIASAEQATAFADADIBfBMIAVALQAYAMAfAIIgEACQAXgDAJgYQAzgnAwhNIAJgHIADgBQAKgDAOAFQAMAFAIAJQAHAHAFALQg1CxhuAYIgXAEQgvAGhPg0IgrgdQgaDVAKCBg");
	this.shape_8.setTransform(100,94.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F5EF79").s().p("AjSEDIAOlmIACgrQAEgdgNgZQgLgWgXgOIgHgDQA0AIAlALIASADQAUAGACACIBgBLIATALQAaAMAgAIIgGACQAXgFAJgYQAvgrAthPIAJgHIACgBQAKgEAPAFQALAEAJAIQAIAHAFAKQgtC3huAcIgVAEQgvAIhRg0IgsgbQgbDUALCBg");
	this.shape_9.setTransform(99.6,94.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F5EF79").s().p("AjPEIIAPlmIABgrQAFgdgOgYQgMgXgWgNIgGgDQAzAHAlALIASADQAUAFADADIBgBKIATAKQAcAMAdAHIgEACQAWgEAGgaQAugtAphRIAHgJIADAAQAJgFAPAEQAMAEAKAIQAIAGAFAKQglC7hrAfIgVAGQguAJhUgzIgugaQgaDUAKCBg");
	this.shape_10.setTransform(99.3,93.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F5EF79").s().p("AjLEMIAPllIABgrQAFgdgOgZQgMgWgWgNIgGgDQAzAHAlAKIATAEQATAEADADIBhBJIATAKQAbALAfAHIgGADQAWgFAFgbQArgvAkhUIAIgJIACgBQAJgFAPADQAMADAKAIQAJAGAFAKQgcC/hpAjIgVAGQgtAIhWgvIgvgaQgbDVALCBg");
	this.shape_11.setTransform(98.8,93.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F5EF79").s().p("AjGERIAOlmIACgrQAEgdgNgYQgNgWgVgNIgIgDQA0AGAmALIASADQATAFACACIBiBIIAUAJQAbAMAeAGIgFADQAVgGADgbQApgyAfhVIAIgJIACgBQAJgGAPACQAMADAKAHQAKAFAFAKQgUDDhnAmIgUAGQgsAKhZguIgwgYQgbDUALCBg");
	this.shape_12.setTransform(98.4,92.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#F5EF79").s().p("AjCEVIAPllIABgrQAFgdgOgYQgMgXgXgMIgGgDQAzAGAlAKIATAEQATAEADACIBjBHIASAJQAcALAfAGQgHAEABAAQAVgIABgbQAmg0AbhXIAGgJIADgBQAIgHAPACQAMABAMAHQAIAFAHAJQgLDIhlAoIgTAHQgsALhbgtIgygXQgaDVAKCBg");
	this.shape_13.setTransform(98,92.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F5EF79").s().p("Ai9EZIAOllIACgrQAEgdgNgYQgNgWgWgNIgHgDQA0AGAlAKIATADQATAFACABIBiBGIAVAJQAcALAeAGIgGAEQAVgIgBgdQAjg2AWhYIAGgJIACgBQAJgIAPACQAMAAALAGQAKAFAHAJQgDDKhiArIgTAIQgrANhdgsIgzgWQgbDVALCBg");
	this.shape_14.setTransform(97.5,92.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F5EF79").s().p("Ai5EdIAPlmIABgrQAFgdgOgWQgNgXgWgMIgGgEQAzAGAlAKIATADIAVAFIBiBGIAWAIQAcALAfAFIgHAEQAUgHgDgeQAhg3ARhaIAFgKIACgBQAJgIAPABQAMgBALAGQAKAEAIAJQAGDNhfAuIgTAIQgpANhggqIg0gUQgbDUALCBg");
	this.shape_15.setTransform(97,91.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F5EF79").s().p("Ai8EcIAOllIACgrQAEgdgOgXQgMgXgWgMIgHgDQAzAGAmAJIASADIAWAGIBiBDIAWAJQAcAKAfAFIgHAEQAVgHgCgdQAig3AVhYIAGgKIACgBQAIgHAPAAQANABALAGQAKAEAHAJQABDKhhAtIgUAIQgqAOhegqIg1gUQgbDVALCBg");
	this.shape_16.setTransform(97.4,91.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#F5EF79").s().p("Ai/EbIAOllIACgrQAEgdgOgXQgMgWgXgNIgGgDQAzAGAmAJIASADIAVAFIBjBDIAWAJQAcAKAeAFIgFADQAVgHAAgcQAkg2AXhXIAGgKIADgBQAIgHAPABQAMABAMAGQAIAFAIAJQgEDHhjAtIgUAIQgrANhcgqIg2gTQgbDVALCBg");
	this.shape_17.setTransform(97.7,91.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#F5EF79").s().p("AjDEaIAPllIABgrQAEgdgOgXQgMgWgXgMIgGgDQAzAFAmAJIASADIAWAFIBlBCIATAJQAdAJAfAFIgGADQAVgHACgcQAlgzAbhYIAGgJIADgBQAIgGAQABQALABALAHQAKAFAGAJQgIDFhlArIgUAIQgrAMhcgpIg3gSQgaDVAKCBg");
	this.shape_18.setTransform(98.1,92);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#F5EF79").s().p("AjGEaIAOlmIACgrQAEgdgOgXQgMgVgXgNIgHgDQA0AFAlAKIATACIAVAFIBmBCIAUAIQAcAIAfAFIgFAEQAWgIACgaQAog0AchVIAHgJIADgCQAJgFAPABQAMACAKAHQAKAGAGAJQgODBhmArIgUAIQgsANhagqIg4gRQgbDUALCBg");
	this.shape_19.setTransform(98.4,92.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F5EF79").s().p("AjKEZIAPlmIABgrQAFgdgOgWQgOgXgWgLIgGgDQAzAEAmAKIASACIAVAEIBnBBIATAIQAdAJAfAEIgFADQAWgHAEgZQApgyAghVIAHgJIADgBQAJgGAPADQAMADAKAGQAJAGAGAJQgTC+hnAqIgVAIQgtANhYgqIg4gQQgbDUALCBg");
	this.shape_20.setTransform(98.7,92.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#F5EF79").s().p("AjNEXIAPllIABgrQAFgdgOgWQgOgWgWgMIgHgDQA0AEAmAJIASADIAVAEIBnBAIAUAIQAcAJAfAEIgEACQAWgGAFgaQArgwAjhUIAIgIIACgBQAJgFAPACQAMADAKAIQAJAGAGAJQgYC7hpAoIgVAHQgtAOhXgqIg5gQQgbDVALCBg");
	this.shape_21.setTransform(99,92.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#F5EF79").s().p("AjQEWIAPlmIABgrQAFgdgOgWQgOgVgWgMIgHgDQA0AEAlAJIATACIAVAEIBnA/IAUAIQAcAJAgADIgEADQAWgHAGgYQAtgvAmhSIAIgJIACgBQAJgFAPAEQAMADAKAIQAJAHAFAJQgcC2hrAoIgVAHQguANhVgqIg6gOQgbDUALCBg");
	this.shape_22.setTransform(99.3,92.4);
	this.shape_22._off = true;

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#F5EF79").s().p("Ai8EcIAPllIABgrQAFgdgOgXQgNgXgWgMIgHgDQA0AGAlAJIATADIAVAGIBjBEIAVAIQAcAKAfAGIgGAEQAUgIgBgdQAhg2AUhZIAGgKIACgBQAJgIAPABQANAAAKAGQAKAFAHAIQACDLhhAtIgTAJQgqANhegqIg1gUQgbDVALCBg");
	this.shape_23.setTransform(97.3,91.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#F5EF79").s().p("Ai/EcIAPlmIABgrQAFgdgOgXQgNgVgWgNIgHgDQA0AFAlAKIATADIAVAFIBjBDIAVAJQAcAKAfAFIgGADQAVgHAAgdQAjg1AXhYIAGgKIACgBQAJgHAPABQAMABALAGQAJAFAIAIQgDDJhiAsIgUAIQgqANhegpIg1gTQgbDUALCBg");
	this.shape_24.setTransform(97.6,91.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F5EF79").s().p("AjCEbIAPlmIABgrQAFgdgOgWQgNgXgWgMIgHgDQAzAFAmAKIASADIAWAFIBkBCIAVAJQAcAJAfAFIgGAEQAVgIABgcQAlg0AZhXIAHgKIACgBQAIgHAQACQAMABALAHQAKAEAGAJQgHDGhkAsIgUAIQgrAMhcgpIg2gSQgbDUALCBg");
	this.shape_25.setTransform(97.9,91.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F5EF79").s().p("AjEEaIAOlmIACgrQAEgdgOgWQgMgXgXgMIgHgDQAzAGAmAJIATADIAUAEIBmBCIAUAIQAcAKAfAFIgFADQAVgIACgbQAngzAchXIAHgJIACgBQAIgGAQACQAMABAKAHQAKAFAGAJQgLDDhlArIgVAIQgrANhbgqIg3gRQgbDUALCBg");
	this.shape_26.setTransform(98.2,92);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F5EF79").s().p("AjHEZIAOllIACgrQAEgdgOgXQgOgVgVgNIgHgDQAzAFAmAJIATADIAUAFIBnBBIAUAIQAcAJAfAEIgFAEQAVgIAEgaQAogyAehWIAHgJIADgBQAIgGAPACQAMACALAHQAJAGAGAJQgQDAhmAqIgVAIQgrANhagqIg4gRQgbDVALCBg");
	this.shape_27.setTransform(98.5,92.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F5EF79").s().p("AjKEYIAOllIACgrQAEgdgOgWQgOgXgVgLIgHgDQAzAEAmAJIATADIAUAEIBnBBIAUAIQAcAJAfAEIgFADQAXgIAEgZQApgxAhhVIAHgJIACgBQAKgGAPADQAMADAKAHQAJAGAGAJQgUC9hoAqIgVAHQgsANhYgqIg5gQQgbDVALCBg");
	this.shape_28.setTransform(98.8,92.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F5EF79").s().p("AjNEXIAPllIABgrQAEgdgOgWQgNgWgWgMIgHgDQAzAFAmAIIASADIAWAEIBnBAIATAIQAdAIAfAEIgEADQAWgHAFgZQArgwAjhTIAIgJIACgBQAKgGAPAEQAMACAKAIQAIAGAGAKQgYC6hpAoIgVAHQgtANhXgpIg6gQQgaDVAKCBg");
	this.shape_29.setTransform(99.1,92.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(129).to({_off:true},1).wait(40));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(151).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(4));

	// hand
	this.instance = new lib.grampshand("synched",0);
	this.instance.setTransform(127.9,88.7,1,1,8.2,0,0,1.6,9.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(117).to({startPosition:0},0).to({startPosition:0},12).to({regX:1.5,regY:9.5,rotation:-41,x:122.3,y:72.6},7).to({regX:1.6,rotation:-45,x:115.6,y:63.7},8).to({scaleX:1,scaleY:1,rotation:-33.9,x:120.1,y:65.3},7).to({rotation:-52.8,x:115.7,y:64.7},7).to({scaleX:1,scaleY:1,rotation:-32.1,x:120.5,y:65.5},8).wait(4));

	// grandpa shirt right
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E8C4A1").s().p("AgoBMQgcgKgEgXQgBgHACgKQAGgaAbgQIAagLQgVgJAGgUQAHgUAZgCQAYgBAdAWIAMAKIgBAAQgEABADAeIADAbIACAAIgSAVIgQARQgeAegcAAQgLAAgKgDg");
	this.shape_30.setTransform(7.3,89.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#F5EF79").s().p("ACGD5QAIhZgNiZIgOiGIgYAKIAAABIgLAEIAAAAIgHADIAAABIhpAuQhlARhGACQggACgPgCIgCAAIgDgdQgDgeAEgCIABAAQAegKBOgMQBKgLAggOQArgUA+gYIA9gXIAcgJIAXgGIAAAAQAlgJApgGIABAAIAAAAQgaAOgNAbQgNAaAFAdIAQGRg");
	this.shape_31.setTransform(39.8,95.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30}]}).wait(170));

	// grandpa
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#548BC8").s().p("AA7D0QgajjgEhOQgBgVgIgGIgHgBIgcAAIgHABQgHAGgCAVQgEBPgaDiIgcAAIgOibQgOicgCgPQgJgyABgqIAChFIALABIACgBIDeAAQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAOgBQAHBRgQBQQgDAPgOCcIgNCbg");
	this.shape_32.setTransform(69.7,129.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#B2987E").s().p("AASAOQgGgIgNgJQgQgMgJgCQgCgNAXAPQASAMAKALQAHAFgEAIIgFADQABgFgEgFg");
	this.shape_33.setTransform(34.9,39.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E8C4A1").s().p("AAHAyQgpgqgHgbQgHgdAbgMQATgIAUAPQALAHAHAJIAHBAQAAAlgOAAQgIAAgOgOg");
	this.shape_34.setTransform(34.1,40.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#B2987E").s().p("AgZAVQgEgIAHgGQAIgJATgNQAWgPAAALQgIACgQAMQgNAJgGAHQgGAFADAIg");
	this.shape_35.setTransform(102.7,39.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E8C4A1").s().p("AgqAbIAHhAIASgQQAUgPATAIQAbAMgHAdQgIAbgoAqQgOAOgIAAQgOAAAAglg");
	this.shape_36.setTransform(103.4,40.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#50320F").s().p("AhEgoQAJgMAPABIABAAIB1AjIgDAKIh0giQgJAAgFAGQgNAUAhA9IgKAFQgkhDARgZg");
	this.shape_37.setTransform(38.4,39.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#50320F").s().p("AAoAvQAgg7gMgWQgGgHgIABIh0AiIgDgKIB2gjQAPgBAJAMQARAYgkBEg");
	this.shape_38.setTransform(97.8,39.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#A7A9AC").s().p("AAFBLQgQgygogZQgMgOgGgQQgLggAdgGQAogIANgXQAHgLgBgJIAHAAQAJABAIAIQAbAbALBTQAGArgGAdQgLAwgsAAQgBgUgJgZg");
	this.shape_39.setTransform(97.5,28.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#A7A9AC").s().p("AhFBIQgIghAIgnQAUhRAbgbQAOgOAIADIgBAXQAFAXAxAGQAeADgMAhQgGARgMAOQgoAZgQAzQgJAZgBAUQgsgBgMgwg");
	this.shape_40.setTransform(39.1,28.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#A7A9AC").s().p("AguAYQgxgQgFgIIAEgHIAZgUQAigQAuARQAwASAbASQANALAEAGQgjAJgiAAQgmAAgogMg");
	this.shape_41.setTransform(78.4,55.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#A7A9AC").s().p("AhlAbQAEgGAOgLQAbgSAwgSQAugRAiAQQARAJAIALQAHACgDAFQgGAIgxAQQgnAMgnAAQgiAAgjgJg");
	this.shape_42.setTransform(58.8,55.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#391B00").s().p("AhQAiIgDgGQgIgRAJgQIALgPQAVgPAagDQAYgEAXAKQAwAUAMALQAMAKgPANQgSAQgtACIhDAAIgDAAQgUAAgHgGg");
	this.shape_43.setTransform(82.3,155.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#391B00").s().p("AgSAnQgugCgRgQQgRgQBPgjQATgLAZACQAZADATAPIALAOQAKAQgJASIgDAGQgHAGgWAAIguABIgVgBg");
	this.shape_44.setTransform(57.2,155.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#50320F").s().p("AgKAFIAAgJIAVAAIAAAJg");
	this.shape_45.setTransform(68.1,39.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#50320F").s().p("Ag9BJQgUAAgPgPQgOgOAAgUIAAgvQAAgUAOgOQAPgPAUAAIB7AAQAVAAAOAPQAOAOAAAUIAAAvQAAAUgOAOQgOAPgVAAgAhYgyQgLALAAAQIAAAvQAAAQALALQALALAQAAIB7AAQAQAAALgLQALgKAAgRIABAAIAAgvQgBgQgLgLQgLgLgPAAIh8AAQgQAAgLALg");
	this.shape_46.setTransform(79.9,39.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#50320F").s().p("Ag9BJQgUAAgPgPQgOgOAAgUIAAgvQAAgUAOgOQAPgPAUAAIB7AAQAVAAAOAPQAOAOAAAUIAAAvQAAAUgOAOQgOAPgVAAgAhYgyQgLALAAAQIAAAvQAAAQALALQAMALAPAAIB7AAQAQAAALgLQAMgMgBgPIAAgvQAAgQgLgLQgLgLgQAAIh7AAQgPAAgMALg");
	this.shape_47.setTransform(56.4,39.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#BCBEC0").s().p("AgsAEIACgPIBNAPIAJAIg");
	this.shape_48.setTransform(81.3,34.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#BCBEC0").s().p("AgiAGIBMgTIACAPIhXAMg");
	this.shape_49.setTransform(56.8,34.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#E8C4A1").s().p("AgaAuQgMgMABgQIAAgjQAAgQALgMQAMgLAOAAQAQAAALALQALAMAAAQIAAAjQAAAQgLAMQgLALgQAAQgOAAgMgLg");
	this.shape_50.setTransform(69.7,71.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#3C2415").s().p("AhcgfQASAVAYAKQAZAMAZABQAcAAAYgMQAYgKARgWQgUA+hJABIAAAAQhGAAgWg/g");
	this.shape_51.setTransform(69,57.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#534132").s().p("AgUAVQgJgJAAgMQAAgLAJgJQAJgJALAAQAMAAAJAJQAJAJAAALQAAAMgJAJQgJAJgMAAQgLAAgJgJg");
	this.shape_52.setTransform(79.9,40.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#534132").s().p("AgUAVQgJgJAAgMQAAgLAJgJQAJgJALAAQANAAAIAJQAJAJAAALQAAAMgJAJQgIAJgNAAQgLAAgJgJg");
	this.shape_53.setTransform(57.9,40.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgYAbQgLgLAAgQQAAgOALgNQALgLANAAQAPAAAKALQALANAAAOQAAAQgLALQgKAMgPAAQgNAAgLgMg");
	this.shape_54.setTransform(79.7,40.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgYAbQgLgMAAgPQAAgPALgMQALgLANAAQAOAAALALQALAMAAAPQAAAPgLAMQgLAMgOAAQgNAAgLgMg");
	this.shape_55.setTransform(57.7,40.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#CFAF8B").s().p("AABATQgLAAgJgHQgJgGAAgIQAAgKAKgGQgCADAAAFQAAAKAJAEQAJAHAKAAQAJAAAMgGQgKAOgQAAIgCAAg");
	this.shape_56.setTransform(68.6,49.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#E8C4A1").s().p("Aj8D1QhAhUAAihQAAiRBXhjQBZhmCMAAQCNAABZBmQBXBjAACRQAACpg2BMQhHBmjAAAQiuAAhOhmg");
	this.shape_57.setTransform(68.7,34.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#A7A9AC").s().p("AgHD+Qg+gGiRgxQg9gVghgiIgVgeQgbgkABgXQAAgPgKgfIgJgbQgHg1AHg1QAEgaAFgPQAGgQADgTQAFgaAcgTQAigZAeArIIHAAQAdgrAjAZQAbATAFAaQAEATAGAQQAFAPAEAaQAHA1gHA2IgJAaQgKAfAAAPQAAAXgbAkQgJATgYAXQgvAthFAXQhbAfhGAAIgagBg");
	this.shape_58.setTransform(68.6,42);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FDFDFE").s().p("AhRDVIgNlJQgFgdANgaQANgbAZgOIADADIgCgDIAJAIIAHAEIAJAEIABAAIAGACIAJAAIACABIAAgBIAHAAIAGgCIABAAIAHgDIACgBIAGgEIAJgHIgBACIADgDIASAEQAVANAMAYQANAagFAdIgNFJg");
	this.shape_59.setTransform(70.5,91.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32}]}).wait(170));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,144,159.6);


(lib.dadsvg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.dadeyes("synched",0);
	this.instance.setTransform(31.2,32.4,1,1,0,0,0,9.9,2.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition:0},0).to({x:30.8},11).wait(130));

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#503212").s().p("AgOAvQgLgMgEgPQgOgiADgmQAAgMAGgMQAGAOAVAWQAVAWAGAMQAIANAFAbQAFAgAEALQgqgOgOgQg");
	this.shape.setTransform(23.8,21.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#503212").s().p("AgcAeQgDgPAEgPQAHgkAUggQAHgKALgHQgDAQAHAcQAIAgAAALQgBARgKAaQgNAegCALQgbgjgFgVg");
	this.shape_1.setTransform(17.6,23.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#503212").s().p("ABfBRQhUgIgegLQgpgPg3guQhBg2gagOQA1gVAdgIQAugMAkAEQAmAGAjAVQBXAxA8BNQAVAZAIAgQglgThLgGg");
	this.shape_2.setTransform(40.8,11.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CE233D").s().p("AhZCiQgDgyAAhmIAAhaIgpBAIgig2IA9hGQAogPAogFIABAUQAGAVATAAQASgBAIgVIAEgUQArAHAjAOIA8BEIgWA2IgyhBIAGBbQAGBogFAyg");
	this.shape_3.setTransform(31.7,69.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E8C4A1").s().p("AAEAcQgJgbgCABQgEABAFAVQAFAWgFACQgHADgGgbQgGgagBAAQgDAAgGAKQgEAKgHgEQgGgDAIgOQAKgVABgEQACgKAKgIIAEgCIAPgDQALADAXAaQAJAKAFAMQAGAKgDADQgBABgKgOQgKgNgHAEQgDABAOAXQAPAZgGACQgFADgNgXQgMgWgCAAQgEABALAZQALAYgFACIgCAAQgIAAgIgYg");
	this.shape_4.setTransform(58.1,87.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E8C4A1").s().p("AA4CSIhCiRIAAgBQgJgQgdglIgkguIAFgvIArA4QArA8ALAYIABABQAHAPAeA9IAdA8QABAFgOAHQgIAEgEAAQgBAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAg");
	this.shape_5.setTransform(50.6,71.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E8C4A1").s().p("AgVA0QgFgCALgYQAMgZgEgBQgCAAgNAVQgNAYgFgDQgFgDAOgYQAOgXgDgBQgGgEgLANQgKAOgBgBQgDgDAGgKQAGgMAIgKQAYgaALgDIAPAEIAEACQAJAGADAMQABAEAKAVQAIAOgGADQgIAEgEgKQgGgKgCAAQgCAAgGAZQgGAbgGgCQgFgCAFgWQAEgWgDgBQgCgBgJAcQgKAXgHAAIgCAAg");
	this.shape_6.setTransform(5,87.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E8C4A1").s().p("AhICOQgOgIABgEQAAgDAdg5IAmhMIABgBQAMgYArg8IAsg3IAEAvIglAuQgdAlgJAQIAAABQgKAWg6B6QAAAAAAABQAAAAgBAAQAAAAgBAAQAAABgBAAQgEAAgIgFg");
	this.shape_7.setTransform(12.8,71.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#1B469D").s().p("AA8CTQADhAgIhBQgOh/gvgDQgkgDgKCAQgFBAACBBQgWAIgTgFIgPgHIAPkgIC2AAIAaEZQgKAUgWAAg");
	this.shape_8.setTransform(32.5,98.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E8C4A1").s().p("ABWCUIgVgHQAGhAgHhAQgNh+g4ADQg7ADgIB2QgEA8AHA7QgMAVgNgHIgLgMIAJkXIC2AAIATEPQACAYgSAAIgDAAg");
	this.shape_9.setTransform(32.4,98.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#503212").s().p("Aj2BhQgfhNAghAQAag1A4gLQAbgGAXAFQAVgXAogPQBOgdBdAqQBzAzAWB0QAMA6gMAxQhKALhRgMQifgXgjhyQAGAagLAhQgXBBhYAiQgWgZgPgmg");
	this.shape_10.setTransform(30.4,16);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3C2415").s().p("Ag4AYIgCgEQgEgJACgKQACgIAIgGQAPgLASgDQARgCAQAHQAhAPAIAHQAJAIgKAIQgPAJgfACIgtABIgDAAQgNAAgFgEg");
	this.shape_11.setTransform(42.7,113.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3C2415").s().p("AgNAcQgggBgLgNQgIgKALgGQAKgIAegLQAfgOAfAUQAIAHACAIQADAJgEAJIgCAFQgGAEgPAAIgmABIgKAAg");
	this.shape_12.setTransform(21.9,113.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#B2987E").s().p("AAFASQgNgCgJgYQgGgPARAKQAOAJAIAIQAFAFgDAFQgFAEgGAAIgCAAg");
	this.shape_13.setTransform(9.5,31.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E8C4A1").s().p("AAFAjQgbgdgGgTQgFgUATgIQANgGAOAKQAHAFAFAHIAFAsQAAAagKAAQgFAAgKgKg");
	this.shape_14.setTransform(8.3,32);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#B2987E").s().p("AgRAOQgDgFAEgFQAIgIAOgJQARgKgFAPQgKAYgNACg");
	this.shape_15.setTransform(53.9,31.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E8C4A1").s().p("AgdATIAFgsIAMgMQAOgKANAGQATAIgFAUQgFATgcAdQgKAKgFAAQgKAAAAgag");
	this.shape_16.setTransform(55,32);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E8C4A1").s().p("AgRAgQgIgHgBgMIAAgYQAAgLAIgJQAJgIAJAAQALABAIAIQAIAIAAALIAAAYQgBALgHAHQgHAIgLAAIgBABQgJAAgIgIg");
	this.shape_17.setTransform(31.7,54.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#50320F").s().p("AgugbQAGgIALABIAAAAIBPAXIgCAHIhOgXQgGAAgEAEQgJANAXAqIgHADQgZguAMgQg");
	this.shape_18.setTransform(10.7,32.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#50320F").s().p("AAcAgQAWgpgJgNQgEgFgGABIhOAXIgCgHIBQgYQAKgBAGAIQALARgYAug");
	this.shape_19.setTransform(51.2,32.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#50320F").s().p("AgGADIAAgFIANAAIAAAFg");
	this.shape_20.setTransform(31,32);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#50320F").s().p("AAqAxIhTAAQgOAAgKgJQgKgKAAgOIAAgfQABgOAJgKQAKgJAOAAIBTAAQAOAAAKAJQAKAKAAAOIAAAfQAAAOgKAKQgJAJgNAAIgCAAgAg8giQgHAIAAALIAAAfQAAALAHAIQAIAHALAAIBTAAQALAAAIgHQAHgIAAgLIAAgfQAAgLgHgHQgIgIgLAAIhTAAQgLAAgIAHg");
	this.shape_21.setTransform(39,32.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#50320F").s().p("AArAxIhUAAQgOAAgJgJQgKgKAAgOIAAgfQAAgOAKgKQAJgJAOAAIBUAAQANAAAKAJQAKAKgBAOIAAAfQABAOgKAKQgKAJgMAAIgBAAgAg8giQgHAIAAALIAAAfQAAALAHAIQAIAHALAAIBUAAQAKAAAIgHQAIgIAAgLIAAgfQAAgLgIgIQgIgHgKAAIhUAAQgLAAgIAHg");
	this.shape_22.setTransform(23,32.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#754C29").s().p("AgiAFIBMgSIACAPIhXALg");
	this.shape_23.setTransform(23,27.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#3C2415").s().p("AhBgVIAQALQAUAKAbAAQAlAAAegVQgOArgyAAIgBAAQgxAAgQgrg");
	this.shape_24.setTransform(31.8,45.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgQATQgIgIAAgLQAAgKAIgIQAHgIAJAAQAKAAAIAIQAHAIAAAKQAAALgHAIQgIAIgKAAQgJAAgHgIg");
	this.shape_25.setTransform(39,32.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgQATQgIgIAAgLQAAgKAIgIQAHgIAJAAQAKAAAIAIQAHAIAAAKQAAALgHAIQgIAIgKAAQgJAAgHgIg");
	this.shape_26.setTransform(23.5,32.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CFAF8B").s().p("AAAANQgIAAgIgFQgHgGAAgFQAAgFADgEIAAACQAAAHAHAEQAIAFAIAAQANAAAIgIQgCAGgHAFQgGAEgGAAIgDAAg");
	this.shape_27.setTransform(31.2,39.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E8C4A1").s().p("AhUDqQgsgLgcgSQgsgcgHg2QgBgGAAhrQAAhqA0hHQA4hMBhAAQBgAAA8BIQA4BEAABnQAABqgCAQQgJA7gvAcQgaAQgwAKQgsAJgkAAQgnAAgqgKg");
	this.shape_28.setTransform(31.5,28.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#503212").s().p("AgEChQgsgEhngjQgqgOgXgYIgPgVQgTgZAAgRQgDgYgKgaQgFgmAFglQADgTADgKQAEgLADgOQACgJAVAUQAVAUAYAjIFtAAQAYgjAVgUQAVgUACAJQACAOAFALQADAKADASQAFAmgFAmQgLAcgCAWQAAARgTAZQgHANgRAQQghAhgwAQQg/AVgyAAIgSgBg");
	this.shape_29.setTransform(31,35.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(170));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,63.2,116.3);


(lib.babysvg = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.babyarmL("synched",0);
	this.instance.setTransform(23.7,33.8,1,1,13.4,0,0,0.4,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(52).to({startPosition:0},0).to({regY:6.7,rotation:-11.6,y:33.7},10,cjs.Ease.get(1)).to({regY:6.8,rotation:13.4,y:33.8},10,cjs.Ease.get(-1)).to({_off:true},2).wait(96));

	// Layer 4
	this.instance_1 = new lib.babyarmR("synched",0);
	this.instance_1.setTransform(13.8,35.4,1,1,-12.5,0,0,6.4,4.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(52).to({startPosition:0},0).to({rotation:12,x:11.9},10,cjs.Ease.get(1)).to({rotation:-12.5,x:13.8},10,cjs.Ease.get(-1)).to({_off:true},2).wait(96));

	// Armature_17
	this.instance_2 = new lib.babytorso("synched",0);
	this.instance_2.setTransform(19.7,33.7,1,1,0,0,0,8.2,3.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off:true},74).wait(96));

	// eyes
	this.instance_3 = new lib.babyeyes("synched",0);
	this.instance_3.setTransform(21.1,16.7,1,1,0,0,0,7.2,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(39).to({startPosition:0},0).to({x:20.9,y:16.6},13).to({_off:true},22).wait(96));

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C064A7").s().p("AgHAIQgDgDgBgFQABgEADgDQADgEAEAAQAFAAADAEQADADABAEQgBAEgDAEQgDAEgFAAQgEAAgDgEg");
	this.shape.setTransform(20,40.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#A065AA").s().p("AABANQgDgCgEgFQgFgFAAgEQgBgFADgDQADgCAFABQAEACAEAFQAEAFABAEQABAFgDADQgCABgDAAIgEAAg");
	this.shape_1.setTransform(18.3,38.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#A065AA").s().p("AAAAJQgGAAgEgEQgFgDAAgCQABgEAFgDQAFgCAEAAQAIABADADQAFAEAAACQgBAEgFACQgEADgEAAIgCgBg");
	this.shape_2.setTransform(17.4,40.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A065AA").s().p("AgLAKQgCgDABgFQACgEAFgEQAFgEAEgBQAFgBADADQACAEgBAFQgCADgFAEQgFAFgEAAIgCAAQgEAAgCgCg");
	this.shape_3.setTransform(17.9,42);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#A065AA").s().p("AgGAKQgDgEAAgGQABgGADgEQADgEACgBQAEAAADAGQADAFgBAEQAAAGgDAFQgDAFgDAAQgDgBgDgFg");
	this.shape_4.setTransform(19.8,42.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#A065AA").s().p("AACANQgDgCgFgEQgEgFgBgDQgCgGADgDQADgDAGACQADABAFAEQAEAGABADQABAFgDADQgCACgDAAIgDAAg");
	this.shape_5.setTransform(21.6,42.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A065AA").s().p("AgKAHQgFgDAAgEQAAgCAFgEQAFgDAFAAQAGAAAEADQAGAEAAACQAAAEgGADQgEADgGgBQgFABgFgDg");
	this.shape_6.setTransform(22.5,40.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#A065AA").s().p("AgKALQgDgDACgFQABgEAEgEQAFgFADgBQAGgCADADQADADgCAGQgBADgFAFQgEAFgDABIgDAAQgEAAgCgCg");
	this.shape_7.setTransform(21.6,38.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#A065AA").s().p("AgFALQgEgFAAgGQAAgFAEgFQACgEADAAQAEAAACAEQADAFABAFQgBAGgDAFQgCAEgEAAQgDAAgCgEg");
	this.shape_8.setTransform(20,37.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C064A7").s().p("AgKALQgFgEAAgHQAAgFAFgFQAEgFAGAAQAGAAAFAFQAFAFAAAFQAAAHgFAEQgFAFgGAAQgFAAgFgFg");
	this.shape_9.setTransform(11.6,8.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#A065AA").s().p("AADASQgGgCgGgHQgGgIgBgGQgCgHAFgEQAEgEAIADQAFACAGAIQAHAHABAGQABAHgFAEQgCACgEAAIgFgBg");
	this.shape_10.setTransform(9.3,6.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#A065AA").s().p("AAAANQgJAAgGgFQgHgFABgEQAAgFAIgEQAGgDAIAAQAKABAFAFQAHAFgBADQAAAGgHAEQgFADgHAAIgDgBg");
	this.shape_11.setTransform(8.1,8.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#A065AA").s().p("AgQAOQgEgEADgIQACgFAIgGQAHgHAGgBQAHgBAEAFQAEAEgDAHQgCAGgHAGQgIAGgFACIgDAAQgGAAgDgEg");
	this.shape_12.setTransform(8.9,11.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#A065AA").s().p("AgJAQQgEgHABgJQAAgIAEgHQAEgHAEABQAGAAAEAHQAEAGAAAIQgBAJgEAHQgEAHgFAAQgFgBgEgGg");
	this.shape_13.setTransform(11.6,12.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#A065AA").s().p("AAEASQgGgCgHgHQgGgGgCgGQgCgIAFgEQAEgEAHACQAGACAHAHQAGAHACAGQABAHgEAEQgCADgEAAIgFgBg");
	this.shape_14.setTransform(14.2,11.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#A065AA").s().p("AgPAJQgGgEgBgFQABgEAGgEQAHgEAIAAQAKAAAGAEQAHAEAAAEQAAAFgHAEQgHAEgJAAQgIAAgHgEg");
	this.shape_15.setTransform(15.3,8.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#A065AA").s().p("AgPAQQgEgEACgIQACgFAHgHQAHgHAFgCQAIgCAEAEQAEAEgCAIQgCAFgHAHQgHAHgFACIgFAAQgEAAgDgCg");
	this.shape_16.setTransform(14.2,6.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#A065AA").s().p("AgIAQQgEgHAAgJQAAgIAEgHQAEgHAEAAQAFAAAEAHQAEAHAAAIQAAAKgEAGQgEAHgFgBQgEABgEgHg");
	this.shape_17.setTransform(11.6,5.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#854E9F").s().p("Ah4AbIDFhkIAuAZIj1B6g");
	this.shape_18.setTransform(14.9,7.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#854E9F").s().p("AgdA5QgMgJAEgOQAEgNALgEQAHgEgCgcQgDgnABgFIA3ANQgJAVAHAZQAHAYgDAFQgHANgYALQgOAHgKAAQgHAAgFgDg");
	this.shape_19.setTransform(15.2,54);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#854E9F").s().p("AgDA2QgZgJgIgNQgCgEAEgaQAFgYgLgVIA2gQQABAEAAAoQAAAcAIADQAMAEAFAMQAEAOgLAJQgFAFgJAAQgKAAgMgGg");
	this.shape_20.setTransform(24.8,53.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#534132").s().p("AgfACQgOgGADgHQAIAIAMACQAIABAOAAIATgBQANgCALgIQgKAXgiAAQgQgBgOgJg");
	this.shape_21.setTransform(20.9,27.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgGASQgHgDgDgIQgDgGADgHQADgHAHgEQAGgDAHADQAHADADAHQADAHgDAHQgDAIgHACQgEACgDAAIgGgBg");
	this.shape_22.setTransform(26.8,16.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgGASQgHgDgDgIQgDgHADgFQADgIAHgEQAGgDAHADQAHADADAIQADAGgDAGQgDAJgHACQgEACgDAAIgGgBg");
	this.shape_23.setTransform(15.5,17.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#754C29").s().p("AgPAPQAAgGAEgJQAGgNANgUQADgGAGgDQgEAIAAAOIgDAVQgCAIgIANIgJAVQgHgUABgIg");
	this.shape_24.setTransform(11.7,10.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#754C29").s().p("AhGAjQAAgeAIgMQAJgQAQgNQAkgfArgNQATgFAKACQgPANgcA6QgcA6gKAMQgMAPgPAAQgRAAgNALIgDgxg");
	this.shape_25.setTransform(9.6,9.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#754C29").s().p("AA6A3Qg5gFgegLQgPgGglgZQghgVgTgDQAQgOAbgNQAagMAUgFQAcgEAiAMIAkAMQAEACAfAXQASAPANAdIAOAhIhMgHg");
	this.shape_26.setTransform(21.1,6.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#754C29").s().p("AgIACIAEgDQAEgIAFgDIAEgDIgDAJIgBAIQgDAFgGAKQgEgHAAgIg");
	this.shape_27.setTransform(9.5,2.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#754C29").s().p("AAPASQgPgGgFgEQgIgFgJgLQgKgPgEgEQAXgDANAEQAEACAIAHQAQAQAHARQADAEgBAJQgGgGgQgFg");
	this.shape_28.setTransform(33.2,13.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CFAF8B").s().p("AgGAJQAAAAAAgBQAAAAAAAAQABAAAAgBQABAAABAAQADgCACgDQABgCgCgDIgBgFQABgBAEAFQAEAEgCADQgCADgFADIgFABIgBgBg");
	this.shape_29.setTransform(21.7,23);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E8C4A1").s().p("Ag5B+QgVgOgLgKQgNgMgMgVQgngDgHgYQgHgXAYgFQgRhDAxgzQA4g6BlATQB4AWgDBuQgBAngUApQgTAngXAPQgkAXgqACIgHAAQgnAAghgWg");
	this.shape_30.setTransform(17.8,16.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#754C29").s().p("AhFAkQgBgfAIgMQAHgOARgPQAmgfApgMQAQgFANABQgPANgbA6QgdA7gKALQgMAPgOABQgSAAgMAKIgCgwg");
	this.shape_31.setTransform(10.9,20.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#754C29").s().p("AA6A3Qg5gGgegLQgQgGgkgYQghgVgTgEQAQgNAbgNQAagMATgEQAdgFAiANIAkAMQAEACAeAXQATAOAOAdQAHAPAGASIhMgHg");
	this.shape_32.setTransform(22.3,17.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#754C29").s().p("AgOAUQACgQADgGQADgIAHgMIAOgTIAEAPQACAIgBAHQgCAIgEAGQgHAPgQARIgKAHQADgGACgQg");
	this.shape_33.setTransform(2.1,17.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[]},74).wait(96));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.4,60.1);


// stage content:



(lib.scene1 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		canvas.style.backgroundColor="rgba(0,0,0,0)";
		document.body.style.backgroundColor = "rgba(0,0,0,0)";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(170));

	// sis
	this.instance = new lib.daughtersvg("synched",0);
	this.instance.setTransform(207.6,96.4,1.209,1.209,0,0,0,26.9,36.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(170));

	// bro
	this.instance_1 = new lib.sonsvg("synched",0);
	this.instance_1.setTransform(241.1,95.9,0.808,0.808,0,0,0,43,50.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(170));

	// baby
	this.instance_2 = new lib.babysvg("synched",0);
	this.instance_2.setTransform(150.3,99.8,1.22,1.22,0,0,0,19.7,29.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(74).to({startPosition:0},0).to({regX:19.6,rotation:-7,x:150.8,y:92.9},11,cjs.Ease.get(1)).to({regX:19.7,rotation:0,x:150.3,y:99.8},11,cjs.Ease.get(-1)).to({rotation:7.2,x:150.9,y:92.9},10,cjs.Ease.get(1)).to({regX:0,regY:0,rotation:0,x:126.3,y:63.4},12).wait(52));

	// dad
	this.instance_3 = new lib.dadsvg("synched",0);
	this.instance_3.setTransform(121,70.7,1.124,1.124,0,0,0,31.6,58.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(170));

	// mom
	this.instance_4 = new lib.momsvg("synched",0);
	this.instance_4.setTransform(180.7,70.8,1.183,1.183,0,0,0,29.2,54.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(170));

	// gramps
	this.instance_5 = new lib.grandpasvg("synched",0);
	this.instance_5.setTransform(284.3,70.9,0.805,0.805,0,0,0,70.2,79.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(170));

	// seal
	this.instance_6 = new lib.seal();
	this.instance_6.setTransform(175.6,76.6,0.499,0.499,0,0,0,145.9,70.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({scaleX:0.54,scaleY:0.54,x:134.1,y:75.2},15,cjs.Ease.get(1)).to({regY:70.8,scaleX:0.58,scaleY:0.58,x:95.3,y:73.9},14).wait(141));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(260.5,75.4,258.1,134.7);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;