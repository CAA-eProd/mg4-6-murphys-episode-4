

// ..............................................................................

// Project: CAA-4234 | Meet The Murphys - TI Game for Q1
// Date: 1/13/2015
// CAA SCO - E-biz

// ..............................................................................


window.murphys = window.murphys || {};

murphys.shared = {

    init : function () {

        var imageSrcs   = [ "https://www.caasco.com/~/media/OM/murphys-game/images/caa-logo.png" ,
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/animations/intro",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/animations/q1",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/animations/q2",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/animations/q3",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/intro/grass",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/intro/house",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q1/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q1/floor",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q1/wall",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q2/boat-water",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q2/sand",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q3/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q3/floor",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/bg/q3/wall",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/correct",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/next-dark",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/text/murphys-title"
                          ];
        var images      = [ ];

        murphys.shared.ga();
        murphys.shared.preloadImages( imageSrcs, images, murphys.shared.letsStart );

    },

    letsStart : function () {

        $('#loading').hide();
        $('#fullpage').show();

        murphys.shared.fullPage();
        murphys.shared.isMobile();
        murphys.shared.questions();
        murphys.shared.formSettings();
        murphys.shared.formValidation();
        murphys.shared.formFieldAnimation();
        murphys.shared.buttons();
        $.fn.fullpage.reBuild();

    },

    isMobile : function () {

        (function(i){var e=/iPhone/i,n=/iPod/i,o=/iPad/i,t=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,r=/Android/i,d=/BlackBerry/i,s=/Opera Mini/i,a=/IEMobile/i,b=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,h=RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),c=function(i,e){return i.test(e)},l=function(i){var l=i||navigator.userAgent;this.apple={phone:c(e,l),ipod:c(n,l),tablet:c(o,l),device:c(e,l)||c(n,l)||c(o,l)},this.android={phone:c(t,l),tablet:!c(t,l)&&c(r,l),device:c(t,l)||c(r,l)},this.other={blackberry:c(d,l),opera:c(s,l),windows:c(a,l),firefox:c(b,l),device:c(d,l)||c(s,l)||c(a,l)||c(b,l)},this.seven_inch=c(h,l),this.any=this.apple.device||this.android.device||this.other.device||this.seven_inch},v=i.isMobile=new l;v.Class=l})(window);

        if (isMobile.apple.phone || isMobile.android.phone || isMobile.android.tablet || isMobile.seven_inch) { murphys.shared.mobileOrientation(); }

    },

    preloadImages : function (srcs, imgs, callback) {

		var img;
		var remaining = srcs.length;


		for (var i = 0; i < srcs.length; i++) {

            img = new Image();

            img.onload = function() {

            --remaining;

            value = (1-(remaining/srcs.length)) * 100;
            stringValue = Math.round(value.toString());


            $( "#perc" ).html( stringValue + "%" );
            if ( (remaining <= 0) && (typeof callback === 'function') ) { callback(); }

        };

		img.src = srcs[i];
		imgs.push(img);

		}

	},


    fullPage : function () {

        // $('#landing p').slimScroll({        });

        //sprite animation to function
        function caaAnimate(theName, theFps, theTotalFrame, animName, theNum, theLoop) {
            var frames = [];
            var num = theNum;

            for (var i = 0; i < num; i++) {
                frames.push( i );
            }

            $(theName).animateSprite({

                fps: theFps,
                columns: theTotalFrame,
                animations: {
                    animName: frames
                },
                loop: theLoop,
                complete: function() {
                }
            });
            $(theName).animateSprite('play', animName);

        }

        caaAnimate(".intro-char", "8", "8", "intro", "8", true);

        $('#fullpage').fullpage({

            // you can set the sections background-color below:
            //                   1         2         3           4           5         6          7          8          9          10
            sectionsColor: ['#7dd47e', '#548c8e', '#5fcc5d', '#4a4c4c', '#7dd47e', '#7dd47e'],

            keyboardScrolling: false,
            scrollingSpeed: 500,
            scrollOverflow: true,
            controlArrows: false,
            verticalCentered: false,
            afterLoad: function(anchorLink, index){

                if(index == 2){
                    caaAnimate(".q1-char", "3", "8", "question1", "8", false);
                }

                if(index == 3){
                    caaAnimate(".q2-char", "6", "8", "question2", "8", false);
                    //$.fn.fullpage.setMouseWheelScrolling(false);
                }

                if(index == 4){
                    caaAnimate(".q3-char", "6", "8", "question3", "8", true);
                }

                if(index == 5){

                }

                if(index == 6){

                }

            }

        });

        //if ($('#form').is(":hidden")){ $.fn.fullpage.setMouseWheelScrolling(false); }
        $.fn.fullpage.setAllowScrolling(false);

    },

    buttons : function () {

        $('.js-go-q1').click( function() { $.fn.fullpage.moveTo(6); });
        $('.js-go-q2').click( function() { $.fn.fullpage.moveTo(3); });
        $('.js-go-q3').click( function() { $.fn.fullpage.moveTo(4); });
        $('.js-go-s5').click( function() { $.fn.fullpage.moveTo(5); });
        $('.js-go-form').click( function() {
            window.location.href = 'user-return';
        });

        $('.btn-tandc').click( function() {
            console.log('terms clicked');
            $('#tandc').show();
        });
        $('#tandc').click( function() {
            $('#tandc').hide();
        });

    },

    questions : function () {

        var q1a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Good idea, but you can do even more. Consider leaving a house key and the phone number of your destination with a trusted individual. Have someone check on your home daily while you are away to ensure that it is safe, and in order to maintain full coverage on your policy.</p>";
        var q1a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Smart, but you can do more. Use timers on lights, radios and televisions, and leave drapes and shades open. Also, don’t forget to pause your newspaper deliver and make arrangements for your mail to be picked up.</p>";
        var q1a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! To learn more tips from CAA Insurance, <a href=\"http://blog.caasco.com/insurance/heres-how-to-discourage-and-protect-your-home-against-burglars/\" target=\"_blank\">click here</a>.</p>";

        var q2a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Sure, but that’s only one idea. Water damage caused by sewer backup is one of the most common threats to Canadian homes. To find out more about how to prevent water damage in your home, <a href=\"http://blog.caasco.com/insurance/how-to-prevent-water-damage-in-your-home/\" target=\"_blank\">click here.</a></p>";
        var q2a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>That’s one idea. Water damage caused by sewer backup is one of the most common threats to Canadian homes. To find out more about how to prevent water damage in your home, <a href=\"http://blog.caasco.com/insurance/how-to-prevent-water-damage-in-your-home/\" target=\"_blank\">click here.</a></p>";
        var q2a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! To find out more everyday tips, <a href=\"http://blog.caasco.com/insurance/how-to-prevent-water-damage-in-your-home/\" target=\"_blank\">click here.</p>";

        var q3a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Nope! Before looking for a contractor, you should call your insurance company to report the damage and loss. A professional insurance representative will assist in getting you the help you need as well as tell you what you can expect from the rest of the process.</p>";
        var q3a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/correct.png\" alt=\"Correct.\"/><p>Correct! You should call your insurance company as soon as possible to report any damage and loss. CAA Home Insurance policies cover water damage under certain circumstances, so you should refer to your homeowner insurance policy or call your insurance company to see how your coverage applies.</p>";
        var q3a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect\"/><p>Put down those tools! Before anything else, you should call your insurance company to report the damage and the loss. A professional insurance representative can have the loss assessed and tell you how you can minimize costs.</p>";


       $( '.cta li' ).click( function () {

		clickedId = eval( this.id );

		if( (clickedId == q1a3) || (clickedId == q2a3) || (clickedId == q3a2) ){

			$( ".answer-container" ).addClass( "correct" );
			$( ".answer-container" ).removeClass( "incorrect" );
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/correct.png\" class\"js-answer-icon\" alt=\"Correct!\"/>");

		} else {

			$( ".answer-container" ).addClass( "incorrect" );
			$( ".answer-container" ).removeClass( "correct" );
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep1-5/images/icons/incorrect.png\" alt=\"Incorrect.\"/>");

		}

		$( ".answer" ).html( clickedId );
		$.fn.fullpage.moveSlideRight();

		});

    },

    formSettings : function () {

        var settings = {

            formId:         "#form",
            lid:            "1474260", //"1434062",
            successPage:    "http://10.100.21.29/OM/MurphysGame/Test/user-return",
            errorPage:      "http://10.100.21.29/OM/MurphysGame/Test/user-return",
            actionMainUrl:  "http://cl.exct.net/subscribe.aspx",
            mid:            "10845500"

        };

        var actionUrl = settings.actionMainUrl + "?lid=" + settings.lid;

        $( "input[name='thx']" ).attr( "value", settings.successPage );
        $( "input[name='err']" ).attr( "value", settings.errorPage );
        $( "input[name='MID']" ).attr( "value", settings.mid );
        $(settings.formId).get(0).setAttribute( 'action', actionUrl );

    },

    formFieldAnimation : function () {

        /*-----
        Shows the form label, in place of the placeholder, when a value has been entered into a field
        -----*/

        $('.field').each(function(){

            var parent = $(this),
                field = parent.find('input, select');

            // Focus: Show label
            field.focus(function(){
              parent.addClass('show-label');
            });

            // Blur: Hide label if no value was entered (go back to placeholder)
            field.blur(function(){
              if (field.val() === '') {
                parent.removeClass('show-label');
              }
            });

            // Handles change without focus/blur action (i.e. form auto-fill)
            field.change(function(){

                if (field.val() !== '') {
                    parent.addClass('show-label');
                } else {
                    parent.removeClass('show-label');
                }

            });

        });

        /*-----
        Extra JS helpers, not directly related to placeholder labels
        -----*/

        // Support placeholders in IE8 via https://github.com/mathiasbynens/jquery-placeholder
        $('input, textarea').placeholder();

        // Add class no-selection class to select elements
        $('select').change(function(){
            var field = $(this);
            if (field.val() === '') {
              field.addClass('no-selection');
            }
            else {
              field.removeClass('no-selection');
            }
        });

    },

    formValidation : function () {

        $('#btn').attr('disabled', 'disabled');

        var isChecked = "n";

        function validateFields(){

            var testEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('#email-address').val();

            if( (!firstName) || (!lastName) || (!testEmail.test(email)) ) {
                $('#btn').attr('disabled', 'disabled');
                $('#btn').addClass('disabled');
            } else {
                $('#btn').removeAttr('disabled');
                $('#btn').removeClass('disabled');
            }

        }

        $('form').keyup(function( event ) { validateFields(); });
        $('input').click(function() { validateFields(); });

        $('#check-more').change(function(){

            isChecked = this.checked ? 'y' : 'n';
            $( "#send-value" ).attr( "value", isChecked );

        });

    },

    mobileOrientation : function () {

        if(typeof window.orientation === 'undefined') {

            var test = window.matchMedia("(orientation: landscape)");

            test.addListener(function(m) {

                if ($('#form').is(":hidden")){

                    if(m.matches) {
                        $('#fullpage').hide();
                        $('#warning-rotate').show();
                    } else {
                        $('#fullpage').show();
                        $('#warning-rotate').hide();
                        $.fn.fullpage.reBuild();
                    }
                }

            });

        } else {

            function statusOrientPage(){

                if( window.orientation != 0 ){
                    $('#fullpage').hide();
                    $('#warning-rotate').show();
                } else {
                    $('#fullpage').show();
                    $('#warning-rotate').hide();
                    $.fn.fullpage.reBuild();
                }

            }

            statusOrientPage();
            window.addEventListener("orientationchange", function() { statusOrientPage(); }, false);

        }

    },

    ga : function (){

		//GA
		//..............................................................
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-9277140-1', 'auto');
		  ga('send', 'pageview');

		  console.log(' GA loaded ');

        // GA tracking events
		//..............................................................
        function gaEvents(argId, argCategory, argAction, argLabel){

            $( argId ).click( function(){
				console.log(argId +" - " + argCategory +" - " + argAction +" - " + argLabel );
                ga('send', 'event', { eventCategory: argCategory, eventAction: argAction, eventLabel: argLabel });
            });

        }

        gaEvents('.js-go-scene1', 'ep1-5 murphys', 'click', 'lets play btn');
        gaEvents('.js-go-scene2', 'ep1-5 murphys', 'click', 'go to intro 2 btn');
        gaEvents('.js-go-scene3', 'ep1-5 murphys', 'click', 'go to intro 3 btn');

        gaEvents('.js-go-q1', 'ep1-5 murphys', 'click', 'go to question 1 btn');
        gaEvents('#q1a1', 'ep1-5 question 1', 'click', 'question 1: alternative #1');
        gaEvents('#q1a2', 'ep1-5 question 1', 'click', 'question 1: alternative #2');
        gaEvents('#q1a3', 'ep1-5 question 1', 'click', 'question 1: alternative #3');

        gaEvents('.js-go-q2', 'murphys', 'click', 'go to question 2 btn');
        gaEvents('#q2a1', 'ep1-5 question 2', 'click', 'question 2: alternative #1');
        gaEvents('#q2a2', 'ep1-5 question 2', 'click', 'question 2: alternative #2');
        gaEvents('#q2a3', 'ep1-5 question 2', 'click', 'question 2: alternative #3');

        gaEvents('.js-go-q3', 'ep1-5 murphys', 'click', 'go to question 3 btn');
        gaEvents('#q3a1', 'ep1-5 question 3', 'click', 'question 3: alternative #1');
        gaEvents('#q3a2', 'ep1-5 question 3', 'click', 'question 3: alternative #2');
        gaEvents('#q3a3', 'ep1-5 question 3', 'click', 'question 3: alternative #3');

        gaEvents('.js-go-form', 'ep1-5 murphys', 'click', 'go to form');

        gaEvents('#form #btn', 'ep1-5 murphys', 'click', 'submit');

        gaEvents('#polguide', 'ep1-5 murphys', 'click', 'terms-conditions-policy-guide lnk');
        gaEvents('#pripol', 'ep1-5 murphys', 'click', 'terms-conditions-privacy-policy lnk');

        gaEvents('.btn-tandc', 'ep1-5 murphys', 'click', 'open-terms-conditions lnk');
    }

};

$(document).ready(function() {  murphys.shared.init();  });


// ..............................................................................