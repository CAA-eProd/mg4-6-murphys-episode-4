

// ..............................................................................

// Project: CAA-4234 | Meet The Murphys - TI Game for Q1
// Date: 1/13/2015
// CAA SCO - E-biz

// ..............................................................................


window.murphys = window.murphys || {};

murphys.shared = {

    init : function () {

        var imageSrcs   = [ "https://www.caasco.com/~/media/OM/murphys-game/images/caa-logo.png" , 
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/intro",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/q1",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/q2",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/q3",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/scene1",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/scene2",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/scene3",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/scene4",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/animations/scene5",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/intro/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/intro/ground-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/intro/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/q1/hotdogs",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/q1/swirl",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/q3/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/q3/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene1/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene1/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene2/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene2/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene3/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene3/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene3/ground-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene4/center",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene4/h-tile",
                            "https://www.caasco.com/~/media/OM/murphys-game/ep2/images/bg/scene5/h-tile"
                          ]; 
        var images      = [ ];

        murphys.shared.ga();
        murphys.shared.preloadImages( imageSrcs, images, murphys.shared.letsStart );

    },

    letsStart : function () {

        $('#loading').hide();
        $('#fullpage').show();
        
        murphys.shared.fullPage();
        murphys.shared.isMobile();
        murphys.shared.questions();        
        
        murphys.shared.formSettings();
        murphys.shared.formValidation();
        murphys.shared.formFieldAnimation();
        murphys.shared.buttons();
        $.fn.fullpage.reBuild();

    },

    isMobile : function () {

        (function(i){var e=/iPhone/i,n=/iPod/i,o=/iPad/i,t=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,r=/Android/i,d=/BlackBerry/i,s=/Opera Mini/i,a=/IEMobile/i,b=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,h=RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),c=function(i,e){return i.test(e)},l=function(i){var l=i||navigator.userAgent;this.apple={phone:c(e,l),ipod:c(n,l),tablet:c(o,l),device:c(e,l)||c(n,l)||c(o,l)},this.android={phone:c(t,l),tablet:!c(t,l)&&c(r,l),device:c(t,l)||c(r,l)},this.other={blackberry:c(d,l),opera:c(s,l),windows:c(a,l),firefox:c(b,l),device:c(d,l)||c(s,l)||c(a,l)||c(b,l)},this.seven_inch=c(h,l),this.any=this.apple.device||this.android.device||this.other.device||this.seven_inch},v=i.isMobile=new l;v.Class=l})(window);
        
        if (isMobile.apple.phone || isMobile.android.phone || isMobile.android.tablet || isMobile.seven_inch) { murphys.shared.mobileOrientation(); }

    },

    preloadImages : function (srcs, imgs, callback) {

		var img;
		var remaining = srcs.length;
		
		
		for (var i = 0; i < srcs.length; i++) {
		
    		img = new Image();

    		img.onload = function() {
    		
    		--remaining;
    		
    		value = (1-(remaining/srcs.length)) * 100;
    		stringValue = Math.round(value.toString());
    		
    		
    		$( "#perc" ).html( stringValue + "%" );
    		if ( (remaining <= 0) && (typeof callback === 'function') ) { callback(); }
    		
		};
		
		img.src = srcs[i];
		imgs.push(img);
		
		}
	
	},


    fullPage : function () {

        // $('#landing p').slimScroll({        });
        
        //sprite animation to function
        function caaAnimate(theName, theFps, theTotalFrame, animName, theNum, theLoop) {
            var frames = [];
            var num = theNum;
            
            for (var i = 0; i < num; i++) {
                frames.push( i );
            }

            $(theName).animateSprite({
                
                fps: theFps,
                columns: theTotalFrame,
                animations: {
                    animName: frames
                },
                loop: theLoop,
                complete: function() {
                }
            });
            $(theName).animateSprite('play', animName);
        
        }


        caaAnimate(".intro-char", "8", "8", "intro", "8", true);
        

		
        $('#fullpage').fullpage({

            // you can set the sections background-color below:
            //                   0         1         2           3           4         5          6          7          8          9          10
            sectionsColor: ['#69a9ab', '#c8b77e', '#4599cf', '#6dac35', '#9ac500', '#e2eeef', '#e2eeef', '#e2dde0', '#1834ac', '#96D269'],

            keyboardScrolling: false,
            scrollingSpeed: 500,
            scrollOverflow: true,
            controlArrows: false,
            verticalCentered: false,
            afterLoad: function(anchorLink, index){

                if(index == 2){
                    caaAnimate(".scene1-char", "5", "8", "scene1", "8", false);
                }

                if(index == 3){
                    caaAnimate(".scene2-char", "5", "8", "scene2", "8", false);
                    //$.fn.fullpage.setMouseWheelScrolling(false);
                }

                if(index == 4){
                    caaAnimate(".scene3-char", "5", "9", "scene3", "9", false);
                }

                if(index == 5){
                    caaAnimate(".question1-char", "5", "8", "question1", "8", true);
                    $(".btn-tandc").addClass("dark");
                }

                if(index == 6){
                   caaAnimate(".scene4-char", "5", "8", "scene4", "8", false);
                   $(".btn-tandc").addClass("dark");
                }

                if(index == 7){
                   caaAnimate(".question2-char", "5", "8", "question2", "8", true);
                }

                if(index == 8){
                   caaAnimate(".question3-char", "5", "14", "question3", "14", false);
                   $(".btn-tandc").addClass("dark");
                }

                if(index == 9){
                   caaAnimate(".scene5-char", "5", "8", "scene5", "8", false);
                }
                if(index == 10){
                   
                }

            }

        });

        //if ($('#form').is(":hidden")){ $.fn.fullpage.setMouseWheelScrolling(false); }
        $.fn.fullpage.setAllowScrolling(false);

    },

    buttons : function () { 

        $('.js-go-scene1').click( function() { $.fn.fullpage.moveTo(2); })
        $('.js-go-scene2').click( function() { $.fn.fullpage.moveTo(3); })
        $('.js-go-scene3').click( function() { $.fn.fullpage.moveTo(4); })
        //$('.js-go-scene4').click( function() {  $.fn.fullpage.moveTo(5); })

        //$('#q-btn').click( function() { $.fn.fullpage.moveTo(5); })

        $('.js-go-question1').click( function() { $.fn.fullpage.moveTo(5); })
        $('.js-go-scene4').click( function() { console.log('clicked'); $.fn.fullpage.moveTo(6); })
        $('.js-go-question2').click( function() { $.fn.fullpage.moveTo(7); })

        $('.js-go-question3').click( function() { $.fn.fullpage.moveTo(8); })
        $('.js-go-scene5').click( function() { $.fn.fullpage.moveTo(9); })
				
        $('.js-go-form').click( function() {
            $.fn.fullpage.moveTo(10);
            // $(".btn-tandc").addClass("dark");
            // function goToForm(){
            //     $("#form").css("display", "inline-block");
                
            // }

            // goToForm();

        })

        $('.btn-tandc').click( function(){ 
            console.log('terms clicked'); 
            $('#tandc').show(); })
        $('#tandc').click( function(){ $('#tandc').hide(); })
 
    },

    questions : function () {
        
        var q1a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Easy, soldier. That won’t be necessary. Call CAA Travel Insurance! They’ll help you find a local doctor’s office and explain how to properly submit your claim.</p>";
        var q1a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>That might help in the short term, but over-the-counter meds aren’t the best solution. Call CAA Travel Insurance! They’ll help you find a local doctor’s office and explain how to properly submit your claim.</p>";
        var q1a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! They’ll help you find a local doctor’s office and explain how to properly submit your claim.</p>";

        var q2a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>That won’t be necessary; this isn’t 1940. Contact CAA Travel Insurance! They’ll help you find a local dental office and explain how to properly submit your claim.</p>";
        var q2a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>This isn’t 1940. Contact CAA Travel Insurance! They’ll help you find a local dental office and explain how to properly submit your claim.</p>";
        var q2a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Correct! One of our friendly consultants<sup>2</sup> will help you find a local dental office and explain how to properly submit your claim.</p>";

        var q3a1 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Wrong! Just give CAA Travel Insurance a call. They’ll help you locate and purchase a replacement set up to $300.<sup>3</sup></p>";
        var q3a2 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/><p>Nope! Just give CAA Travel Insurance a call. They’ll help you locate and purchase a replacement set up to $300.<sup>3</sup></p>";
        var q3a3 = "<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" alt=\"Correct!\"/><p>Right! Just give CAA Travel Insurance a call. They’ll help you locate and purchase a replacement set up to $300.<sup>3</sup></p>";


       $( '.cta li' ).click( function () {

		clickedId = eval( this.id );
		
		if( (clickedId == q1a3) || (clickedId == q2a3) || (clickedId == q3a3) ){ 

			$( ".answer-container" ).addClass( "correct" );
			$( ".answer-container" ).removeClass( "incorrect" );
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/correct.png\" class\"js-answer-icon\" alt=\"Correct!\"/>")
	
		} else {

			$( ".answer-container" ).addClass( "incorrect" );
			$( ".answer-container" ).removeClass( "correct" );	
            $( ".js-answer-icon" ).remove();
            $(".btn-tandc").removeClass("dark");
            $(".answer").append("<img src=\"http://www.caasco.com/~/media/OM/murphys-game/ep2/images/icons/incorrect.png\" alt=\"Incorrect.\"/>")
    
		
		}

		$( ".answer" ).html( clickedId );
		$.fn.fullpage.moveSlideRight();
		
		})

    },

    formSettings : function () {

        var settings = {

            formId:         "#form", 
            lid:            "1399647",
            successPage:    "http://www.caasco.com/OM/MurphysGame/2/user-return",
            errorPage:      "http://www.caasco.com/OM/MurphysGame/2/user-return",
            actionMainUrl:  "http://cl.exct.net/subscribe.aspx",
            mid:            "10845500"

        };

        var actionUrl = settings.actionMainUrl + "?lid=" + settings.lid;

        $( "input[name='thx']" ).attr( "value", settings.successPage );
        $( "input[name='err']" ).attr( "value", settings.errorPage );
        $( "input[name='MID']" ).attr( "value", settings.mid );
        $(settings.formId).get(0).setAttribute( 'action', actionUrl );

    },

    formFieldAnimation : function () {

        /*-----
        Shows the form label, in place of the placeholder, when a value has been entered into a field
        -----*/

        $('.field').each(function(){

            var parent = $(this),
                field = parent.find('input, select');

            // Focus: Show label
            field.focus(function(){
              parent.addClass('show-label');
            });

            // Blur: Hide label if no value was entered (go back to placeholder)
            field.blur(function(){
              if (field.val() === '') {
                parent.removeClass('show-label');
              }
            });

            // Handles change without focus/blur action (i.e. form auto-fill)
            field.change(function(){

                if (field.val() !== '') {
                    parent.addClass('show-label');
                } else {
                    parent.removeClass('show-label');
                }

            });

        });

        /*-----
        Extra JS helpers, not directly related to placeholder labels
        -----*/

        // Support placeholders in IE8 via https://github.com/mathiasbynens/jquery-placeholder
        $('input, textarea').placeholder();

        // Add class no-selection class to select elements
        $('select').change(function(){
            var field = $(this);
            if (field.val() === '') {
              field.addClass('no-selection');
            }
            else {
              field.removeClass('no-selection');
            }
        });

    },

    formValidation : function () {

        $('#btn').attr('disabled', 'disabled');

        var isChecked = "n";

        function validateFields(){

            var testEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var firstName = $('#first-name').val();
            var lastName = $('#last-name').val();
            var email = $('#email-address').val();

            if( (!firstName) 
                || (!lastName)
                || (!testEmail.test(email))
              ) {   
                $('#btn').attr('disabled', 'disabled'); 
                $('#btn').addClass('disabled');  
            } else {   
                $('#btn').removeAttr('disabled'); 
                $('#btn').removeClass('disabled');  
            }

        }

        $('form').keyup(function( event ) { validateFields(); });
        $('input').click(function() { validateFields(); });

        $('#check-more').change(function(){
            
            isChecked = this.checked ? 'y' : 'n';
            $( "#send-value" ).attr( "value", isChecked );

        });

    },

    mobileOrientation : function () { 

        if(typeof window.orientation === 'undefined') {

            var test = window.matchMedia("(orientation: landscape)");

            test.addListener(function(m) {

                if ($('#form').is(":hidden")){

                    if(m.matches) {
                        $('#fullpage').hide();
                        $('#warning-rotate').show();
                    } else {
                        $('#fullpage').show();
                        $('#warning-rotate').hide();
                        $.fn.fullpage.reBuild();
                    }
                }

            });

        } else {

            function statusOrientPage(){

                if( window.orientation != 0 ){
                    $('#fullpage').hide();
                    $('#warning-rotate').show();
                } else {
                    $('#fullpage').show();
                    $('#warning-rotate').hide();
                    $.fn.fullpage.reBuild();
                }

            }

            statusOrientPage();
            window.addEventListener("orientationchange", function() { statusOrientPage(); }, false);

        }

    },

    ga : function (){
		
		//GA
		//..............................................................
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-9277140-1', 'auto');
		  ga('send', 'pageview');
		  
		  console.log(' GA loaded ');
		
        // GA tracking events
		//..............................................................
        function gaEvents(argId, argCategory, argAction, argLabel){

            $( argId ).click( function(){
				console.log(argId +" - " + argCategory +" - " + argAction +" - " + argLabel );
                ga('send', 'event', { eventCategory: argCategory, eventAction: argAction, eventLabel: argLabel });
            });

        }

        gaEvents('.js-go-scene1', 'ep2 murphys', 'click', 'lets play btn');
        gaEvents('.js-go-scene2', 'ep2 murphys', 'click', 'go to intro 2 btn');
        gaEvents('.js-go-scene3', 'ep2 murphys', 'click', 'go to intro 3 btn');

        gaEvents('.js-go-question1', 'ep2 murphys', 'click', 'go to question 1 btn');
        gaEvents('#q1a1', 'ep2 question 1', 'click', 'question 1: alternative #1');
        gaEvents('#q1a2', 'ep2 question 1', 'click', 'question 1: alternative #2');
        gaEvents('#q1a3', 'ep2 question 1', 'click', 'question 1: alternative #3');

        gaEvents('.js-go-question2', 'murphys', 'click', 'go to question 2 btn');
        gaEvents('#q2a1', 'ep2 question 2', 'click', 'question 2: alternative #1');
        gaEvents('#q2a2', 'ep2 question 2', 'click', 'question 2: alternative #2');
        gaEvents('#q2a3', 'ep2 question 2', 'click', 'question 2: alternative #3');

        gaEvents('.js-go-question3', 'ep2 murphys', 'click', 'go to question 3 btn');
        gaEvents('#q3a1', 'ep2 question 3', 'click', 'question 3: alternative #1');
        gaEvents('#q3a2', 'ep2 question 3', 'click', 'question 3: alternative #2');
        gaEvents('#q3a3', 'ep2 question 3', 'click', 'question 3: alternative #3');

        gaEvents('.js-go-form', 'ep2 murphys', 'click', 'go to form');

        gaEvents('#form #btn', 'ep2 murphys', 'click', 'submit');
        
        gaEvents('#polguide', 'ep2 murphys', 'click', 'terms-conditions-policy-guide lnk');
        gaEvents('#pripol', 'ep2 murphys', 'click', 'terms-conditions-privacy-policy lnk');

        gaEvents('.btn-tandc', 'ep2 murphys', 'click', 'open-terms-conditions lnk');
    }

}

$(document).ready(function() {  murphys.shared.init();  });


// ..............................................................................